# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: POST /rest/v1/distributionsets resulting in 200

prepare: |
    # We will be collecting variables as we execute.
    echo >vars.sh

    # This test references what must be created in advance of the excute command. We need to define a
    # software-module-type and a software-module
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/softwaremoduletypes" \
        -d '[{"name": "SysOTA+RAUC Bundle", "maxAssignments": 1, "key": "sysota-rauc-bundle"}]'
           
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'SW_MOD_TYPE_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh

    # Creating a Software-module
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/softwaremodules" \
        -d '[{"vendor": "Oniro", "name": "Oniro Image Base", "type": "sysota-rauc-bundle", "version": "2022.04"}]'

    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'SW_MOD_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh

excute: |
    . vars.sh

    # Creating a distributionsettype with mandatory-software-module-type 
    # TODO: this test uses mandatory software modules, it would be good to extend it to cover various scenarios that involve mandatory and optional modules.
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes" \
        -d '[{"name": "Oniro Distribution-type", "description":"Oniro Distribution description", "key": "oniro","mandatorymodules":[{"id": '"$SW_MOD_TYPE_ID"'}]}]'
        
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'DS_TYPE_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh

    # Creating a distributionset with the distributionsettype above
    # https://www.eclipse.org/hawkbit/rest-api/distributionsets-api-guide/#_post_rest_v1_distributionsets
    curl \
    --verbose \
    --dump-header resp.head \
    --output resp.body \
    -H "$TEST_HAWKBIT_AUTH" \
    -H 'Accept: application/hal+json' \
    -H 'Content-Type: application/json;charset=UTF-8' \
    "${TEST_HAWKBIT_URL}/rest/v1/distributionsets" \
    -d '[{"name": "Oniro Distribution", "description":"Oniro Distribution description", "type": "oniro","version":"one","modules":[{"id": '"$SW_MOD_ID"'}]}]'

    # The request succeeds.
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 201 Created' <resp.head
    GREP -qFx 'Content-Type: application/hal+json;charset=utf-8' <resp.head
    printf 'DS_ID="%s"\n' "$(jq -r '.[0].id' <resp.body)" >>vars.sh

    # Data is the same
    jq '.name' -r < resp.body | GREP -qFx "Oniro Distribution"
    jq '.description' -r < resp.body | GREP -qFx "Oniro Distribution description"
    jq '.type' -r < resp.body | GREP -qFx "oniro"
    jq '.version' -r < resp.body | GREP -qFx "one"
    jq '.deleted' -r < resp.body | GREP -qFx false
    jq '.createdBy' -r < resp.body | GREP -qFx admin
    jq '.lastModifiedBy' -r < resp.body | GREP -qFx admin
    jq '.deleted' -r < resp.body | GREP -qFx false
    jq '._links.self.href' -r < resp.body | GREP -qFx "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/$DS_ID"

restore: |
    . vars.sh
    # Delete the distribution-set we've created earlier.
    if [ -n "${DS_ID:-}" ]; then
        echo "Deleting distribution-set with ID=$DS_ID"

        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/${DS_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsets/${DS_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi

    # Delete the distribution-set-type we've created earlier.
    if [ -n "${DS_TYPE_ID:-}" ]; then
        echo "Deleting distribution-set-type with ID=$DS_TYPE_ID"
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes/${DS_TYPE_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/distributionsettypes/${DS_TYPE_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi

    # Delete the software module we've created earlier.
    if [ -n "${SW_MOD_ID:-}" ]; then
        echo "Deleting software module ID=$SW_MOD_ID"
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/softwaremodules/${SW_MOD_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/softwaremodules/${SW_MOD_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi

    # Delete the software module type we've created earlier.
    if [ -n "${SW_MOD_TYPE_ID:-}" ]; then
        echo "Deleting software module type ID=$SW_MOD_TYPE_ID"
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/softwaremoduletypes/${SW_MOD_TYPE_ID}" -X DELETE

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 200 OK' <resp.head

        # Is it deleted, or soft-deleted? We want really deleted here.
        curl \
            --verbose \
            --dump-header resp.head \
            --output resp.body \
            -H "$TEST_HAWKBIT_AUTH" \
            -H 'Accept: application/hal+json' \
            "${TEST_HAWKBIT_URL}/rest/v1/softwaremoduletypes/${SW_MOD_TYPE_ID}"

        # The request succeeds.
        fromdos resp.head
        GREP -qFx 'HTTP/1.1 404 Not Found' <resp.head
    fi

    rm -f vars.sh