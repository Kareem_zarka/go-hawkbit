# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Zygmunt Krynicki

summary: GET /rest/v1/targets/1001 resulting in 404
execute: |
    # Attempting to retrieve a target that doesn't exist fails in a specific way.
    # https://www.eclipse.org/hawkbit/rest-api/targets-api-guide/#_get_rest_v1_targets_targetid
    curl \
        --verbose \
        --dump-header resp.head \
        --output resp.body \
        -H "$TEST_HAWKBIT_AUTH" \
        -H 'Accept: application/hal+json' \
        -H 'Content-Type: application/json;charset=UTF-8' \
        "${TEST_HAWKBIT_URL}/rest/v1/targets/1001" \

    # The request fails.
    fromdos resp.head
    GREP -qFx 'HTTP/1.1 404 Not Found' < resp.head

    # The returned exception information is sane.
    jq '.exceptionClass' -r < resp.body | GREP -qFx "org.eclipse.hawkbit.repository.exception.EntityNotFoundException"
    # Yes, the response contains a typo.
    # This is issue https://github.com/eclipse/hawkbit/issues/1236
    jq '.errorCode' -r < resp.body | GREP -qFx "hawkbit.server.error.repo.entitiyNotFound"
    jq '.message' -r < resp.body | GREP -qFx "Target with given identifier {1001} does not exist."
