// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package contenttype provides logic for parsing Content-Type.
//
// Content type is defined as media type and an optional list of parameters
// separated by semicolons. The text is entirely case-insensitive and is
// normalized to lowercase. Semicolons may be surrounded by whitespace.
//
// For details about the format see https://httpwg.org/specs/rfc7231.html#media.type
package contenttype

import (
	"bufio"
	"errors"
	"strings"
)

var (
	// ErrUnterminatedDoubleQuote records unterminated double-quoted string.
	ErrUnterminatedDoubleQuote = errors.New("unterminated double quote")
)

// Parse parses the value of a Content-Type header.
func Parse(s string) (mediaType string, opts []string, err error) {
	sc := bufio.NewScanner(strings.NewReader(s))
	sc.Split(splitOpt)

	_ = sc.Scan()
	mediaType = sc.Text()

	for sc.Scan() {
		if len(sc.Bytes()) != 0 {
			opts = append(opts, strings.ToLower(sc.Text()))
		}
	}

	return mediaType, opts, sc.Err()
}

func splitOpt(data []byte, atEof bool) (advance int, token []byte, err error) {
	dq := false
	e := false
	left := 0
	right := len(data)
	advance = len(data)

	skip := true

	for idx, b := range data {
		// Ignore leading whitespace.
		if skip && (b == ' ' || b == '\t') {
			continue
		}

		if skip {
			skip = false
			left = idx
		}

		// If an escape sequence was introduced, skip the next character.
		if e {
			e = false

			continue
		}

		// Double-quoted text is allowed.
		if b == '"' {
			dq = !dq
		}
		// Within double quoted text, '\' introduces an escape sequence.
		if dq && b == '\\' {
			e = true
		}
		// Fields are delimited with semicolons.
		if !dq && b == ';' {
			right = idx
			advance = idx + 1

			break
		}
	}

	if dq {
		// Report an error if we ran out of input while parsing a quoted string.
		if atEof {
			return 0, nil, ErrUnterminatedDoubleQuote
		}

		// Request more input.
		return 0, nil, nil
	}

	// Indicate when the parsing is done.
	if atEof && right == len(data) {
		err = bufio.ErrFinalToken
	}

	return advance, data[left:right], err
}
