// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package contenttype

const (
	AppHalJsonUtf8 = "application/hal+json;charset=utf-8"
	AppHalJson     = "application/hal+json"
	AppJson        = "application/json"
	AppJsonUtf8    = "application/json;charset=utf-8"
)
