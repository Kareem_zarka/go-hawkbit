// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package contenttype_test

import (
	"testing"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"golang.org/x/exp/slices"
)

func TestSmoke(t *testing.T) {
	mediaType, opts, err := contenttype.Parse("text/json")

	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if mediaType != "text/json" {
		t.Fatalf("unexpected media type: %q", mediaType)
	}

	if len(opts) != 0 {
		t.Fatalf("unexpected options: %q", opts)
	}
}

func TestParse(t *testing.T) {
	cases := []struct {
		input     string
		mediaType string
		opts      []string
	}{
		// Examples from https://httpwg.org/specs/rfc7231.html#media.type
		{
			input:     "text/html;charset=utf-8",
			mediaType: "text/html",
			opts:      []string{"charset=utf-8"},
		},

		{
			input:     "text/html;charset=UTF-8",
			mediaType: "text/html",
			opts:      []string{"charset=utf-8"},
		},
		{
			input:     "text/html;Charset=\"UTF-8\"",
			mediaType: "text/html",
			opts:      []string{"charset=\"utf-8\""},
		},
		{
			input:     "text/html; charset=\"utf-8\"",
			mediaType: "text/html",
			opts:      []string{"charset=\"utf-8\""},
		},
		{
			input:     "text/plain; key=value;other=value",
			mediaType: "text/plain",
			opts:      []string{"key=value", "other=value"},
		},
		{
			input:     "text/plain; key=value;\t other=value",
			mediaType: "text/plain",
			opts:      []string{"key=value", "other=value"},
		},
		{
			input:     `text/plain; escape="\""`,
			mediaType: "text/plain",
			opts:      []string{`escape="\""`},
		},
		{
			input:     `text/plain; quoted=";field"`,
			mediaType: "text/plain",
			opts:      []string{`quoted=";field"`},
		},
		{
			input:     `text/plain; ;`,
			mediaType: "text/plain",
		},
	}

	for _, data := range cases {
		mediaType, opts, err := contenttype.Parse(data.input)

		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}

		if mediaType != data.mediaType {
			t.Fatalf("unexpected media type: %q", mediaType)
		}

		if !slices.Equal(opts, data.opts) {
			t.Fatalf("unexpected options %q", opts)
		}
	}
}

func TestUnfinishedQuote(t *testing.T) {
	_, _, err := contenttype.Parse(`text/plain; quote="unfinished`)

	if err != contenttype.ErrUnterminatedDoubleQuote {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestUnfinishedEscape(t *testing.T) {
	_, _, err := contenttype.Parse(`text/plain; quote="\`)

	if err != contenttype.ErrUnterminatedDoubleQuote {
		t.Fatalf("unexpected error: %v", err)
	}
}
