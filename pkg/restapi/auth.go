// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package restapi

import (
	"net/http"

	"gitlab.com/zygoon/go-hawkbit/pkg/headers"
)

// Account implements requestAuthorizer for management APIs.
type Account struct {
	Username string
	Password string
}

// AuthorizeRequest configures basic authentication on the given request.
func (a *Account) AuthorizeRequest(req *http.Request) {
	req.SetBasicAuth(a.Username, a.Password)
}

// TenantAccount implements requestAuthorizer for management APIs when tenants are in use.
type TenantAccount struct {
	Tenant   string
	Username string
	Password string
}

// AuthorizeRequest configures basic authentication on the given request.
func (a *TenantAccount) AuthorizeRequest(req *http.Request) {
	req.SetBasicAuth(a.Tenant+"\\"+a.Username, a.Password)
}

// GatewayToken implements requestAuthorizer for device APIs when gateway authentication is in use.
type GatewayToken string

// String returns the token as a string.
func (tok GatewayToken) String() string {
	return string(tok)
}

// Set sets the token to the given value.
//
// Set implements flag.Value.
func (tok *GatewayToken) Set(s string) error {
	*tok = GatewayToken(s)

	return nil
}

// AuthorizeRequest configures basic authentication on the given request.
func (tok GatewayToken) AuthorizeRequest(req *http.Request) {
	req.Header.Add(headers.Authorization, "GatewayToken "+string(tok))
}

// TargetToken implements requestAuthorizer for device APIs when device authentication is in use.
type TargetToken string

// String returns the token as a string.
func (tok TargetToken) String() string {
	return string(tok)
}

// Set sets the token to the given value.
//
// Set implements flag.Value.
func (tok *TargetToken) Set(s string) error {
	*tok = TargetToken(s)

	return nil
}

// AuthorizeRequest configures basic authentication on the given request.
func (tok TargetToken) AuthorizeRequest(req *http.Request) {
	req.Header.Add(headers.Authorization, "TargetToken "+string(tok))
}
