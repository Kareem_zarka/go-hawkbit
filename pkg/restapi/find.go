// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import (
	"context"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// FindOptions describes how a subset of items is retrieved.
type FindOptions struct {
	Offset int
	Limit  int
	RawQ   string
	Sort   Ordering
}

// FieldOrder describes how to sort a specific field.
type FieldOrder struct {
	Field     string
	Ascending bool
}

// Ordering describes the ordering of a number of fields.
type Ordering []FieldOrder

// Found describes the result of finding resources.
//
// The type parameter manyT represents the resource.
//
// Size and Total describe the sliding window. Unless Size == Total, the
// returned Content is a subset and additional requests are required to obtain
// remaining resources.
type Found[T any] struct {
	Size    int `json:"size"`
	Total   int `json:"total"`
	Content []T `json:"content"`
}

// FindResources retrieves a subset of many resources with the given href.
//
// Options influence the subset of the matching resources, the ordering
// and the particular slice into the search, as computed by the server.
//
// Correct usage of FindResources requires performing the first request, and for as
// long as the number of results retrieved in total is less than the total found
// in the response, to re-issue the request with modified offset.
func FindResources[T any](ctx context.Context, cli *Client, href string, opts FindOptions) (*Found[T], error) {
	u, err := url.Parse(href)
	if err != nil {
		return nil, err
	}

	opts.modifyURL(u)

	href = u.String()

	resp, err := do(ctx, cli, http.MethodGet, href, nil)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, failure(resp)
	}

	return decode[Found[T]](resp)
}

// ModifyURL applies search options to the given URL.
func (opts *FindOptions) modifyURL(u *url.URL) {
	const (
		argLimit  = "limit"
		argOffset = "offset"
		argSort   = "sort"
		argQuery  = "q"
	)

	q := u.Query()

	if opts.Limit != 0 {
		q.Add(argLimit, strconv.Itoa(opts.Limit))
	}

	if opts.Offset != 0 {
		q.Add(argOffset, strconv.Itoa(opts.Offset))
	}

	if len(opts.Sort) != 0 {
		q.Add(argSort, opts.Sort.String())
	}

	if opts.RawQ != "" {
		q.Add(argQuery, opts.RawQ)
	}

	u.RawQuery = q.Encode()
}

// String returns the ordering expressed in a format defined by HawkBit.
func (fo Ordering) String() string {
	var buf strings.Builder

	for i, fo := range fo {
		if i > 0 {
			buf.WriteByte(',')
		}

		buf.WriteString(fo.Field)
		buf.WriteByte(':')

		if fo.Ascending {
			buf.WriteString("ASC")
		} else {
			buf.WriteString("DESC")
		}
	}

	return buf.String()
}
