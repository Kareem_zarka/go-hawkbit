// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

func TestIdRefJsonMarshaling(t *testing.T) {
	data, err := json.Marshal(restapi.IdRef[int]{ID: 42})
	if err != nil {
		t.Fatalf("Cannot marshal JSON: %v", err)
	}

	if string(data) != `{"id":42}` {
		t.Fatalf("Unexpected JSON encoding: %q", data)
	}

	var ref restapi.IdRef[int]

	if err := json.Unmarshal([]byte(`{"id":7}`), &ref); err != nil {
		t.Fatalf("Cannot unmarshal JSON: %v", err)
	}

	if ref != (restapi.IdRef[int]{ID: 7}) {
		t.Fatalf("Unexpected value decoded from JSON: %v", ref)
	}
}
