// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import "fmt"

const (
	fmtCannotCreate   = "cannot create %s"
	fmtCannotRetrieve = "cannot retrieve %s"
	fmtCannotUpdate   = "cannot update %s"
	fmtCannotDelete   = "cannot delete %s"
	fmtCannotPost     = "cannot post %s"
)

// ResourceNamer is an interface for naming resource types.
type ResourceNamer interface {
	ResourceName() string
	ResourcePluralName() string
}

// CreateErrorPrefix provides prefix for errors related to creating objects.
type CreateErrorPrefix[namerT ResourceNamer] struct{}

// ErrorPrefix returns error prefix message.
func (CreateErrorPrefix[namerT]) ErrorPrefix() string {
	var n namerT

	return fmt.Sprintf(fmtCannotCreate, n.ResourceName())
}

// ReadErrorPrefix provides prefix for errors related to retrieving objects.
type ReadErrorPrefix[namerT ResourceNamer] struct{}

// ErrorPrefix returns error prefix message.
func (ReadErrorPrefix[namerT]) ErrorPrefix() string {
	var n namerT

	return fmt.Sprintf(fmtCannotRetrieve, n.ResourceName())
}

// FindErrorPrefix provides prefix for errors related to finding objects.
type FindErrorPrefix[namerT ResourceNamer] struct{}

// ErrorPrefix returns error prefix message.
func (FindErrorPrefix[namerT]) ErrorPrefix() string {
	var n namerT

	return fmt.Sprintf(fmtCannotRetrieve, n.ResourcePluralName())
}

// UpdateErrorPrefix provides prefix for errors related to updating objects.
type UpdateErrorPrefix[namerT ResourceNamer] struct{}

// ErrorPrefix returns error prefix message.
func (UpdateErrorPrefix[namerT]) ErrorPrefix() string {
	var n namerT

	return fmt.Sprintf(fmtCannotUpdate, n.ResourceName())
}

// DeleteErrorPrefix provides prefix for errors related to deleting objects.
type DeleteErrorPrefix[namerT ResourceNamer] struct{}

// ErrorPrefix returns error prefix message.
func (DeleteErrorPrefix[namerT]) ErrorPrefix() string {
	var n namerT

	return fmt.Sprintf(fmtCannotDelete, n.ResourceName())
}

// PostErrorPrefix provides prefix for errors related to posting objects.
type PostErrorPrefix[namerT ResourceNamer] struct{}

// ErrorPrefix returns error prefix message.
func (PostErrorPrefix[namerT]) ErrorPrefix() string {
	var n namerT

	return fmt.Sprintf(fmtCannotPost, n.ResourceName())
}
