// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import (
	"bytes"
	"context"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
)

// Upload uploads a file to the given URL.
func Upload[T any](ctx context.Context, cli *Client, href, path string) (*T, error) {
	contentType, buf, err := prepUpload(path)
	if err != nil {
		return nil, err
	}

	resp, err := do(ctx, cli, http.MethodPost, href, buf, contentType)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusCreated {
		return nil, failure(resp)
	}

	v, err := decode[T](resp)
	if err != nil {
		return nil, err
	}

	return v, nil
}

func prepUpload(path string) (contentType string, buf *bytes.Buffer, err error) {
	buf = new(bytes.Buffer)

	w := multipart.NewWriter(buf)

	defer func() {
		if w != nil {
			_ = w.Close()
		}
	}()

	part, err := w.CreateFormFile("file", filepath.Base(path))
	if err != nil {
		return "", nil, err
	}

	f, err := os.Open(path)
	if err != nil {
		return "", nil, err
	}

	defer func() {
		_ = f.Close()
	}()

	if _, err := io.Copy(part, f); err != nil {
		return "", nil, err
	}

	if err := w.Close(); err != nil {
		w = nil // Disarm deferred close.

		return "", nil, err
	}

	return w.FormDataContentType(), buf, nil
}
