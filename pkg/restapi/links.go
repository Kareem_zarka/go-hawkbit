// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

// Link encodes a single href to a resource.
type Link struct {
	Href      string `json:"href"`
	Templated bool   `json:"templated,omitempty"`
}
