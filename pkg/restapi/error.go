// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package restapi

import (
	"encoding/json"
	"strings"
)

// Apparently there are two error types returned and marshaled by HawkBit.
//
// The more commonly encountered is the Error type defined below, with the
// fields: "errorCode", "exceptionClass", "message" and "parameters".
//
// The second is not explicitly documented, as far as I can tell, and contains
// the fields "timestamp", "status", "error" and "message".
//
// I would guess that the other error type is from the middleware/framework used
// by the web application server, as it was discovered on a request with
// incorrect authorization data.
//
// Missing documentation for this second error type is tracked at
// https://github.com/eclipse/hawkbit/issues/1247

// Error records the error information provided by HawkBit.
//
// https://www.eclipse.org/hawkbit/rest-api/targets-api-guide/#error-body
type Error struct {
	Code       string
	Class      string
	Message    string
	Parameters []string
}

// Error returns the error message.
func (e *Error) Error() string {
	// TODO: return meaningful text if message is empty.
	return e.Message
}

// MarshalJSON implements json.Marshaler.
func (e *Error) MarshalJSON() ([]byte, error) {
	raw := rawError{
		Code:       e.Code,
		Class:      e.Class,
		Message:    e.Message,
		Parameters: e.Parameters,
	}

	return json.Marshal(&raw)
}

// MarshalJSON implements json.Unmarshaler.
//
// The error code is corrected if it contains typos. The exact set of typos is
// unspecified.
func (e *Error) UnmarshalJSON(data []byte) error {
	var raw rawError

	if err := json.Unmarshal(data, &raw); err != nil {
		return err
	}

	// Workaround for https://github.com/eclipse/hawkbit/issues/1238 and related issues.
	e.Code = strings.ReplaceAll(raw.Code, "entitiy", "entity")
	e.Class = raw.Class
	e.Message = raw.Message
	e.Parameters = raw.Parameters

	return nil
}

// rawError is the error returned by HawkBit explicitly.
type rawError struct {
	Code       string   `json:"errorCode"`
	Class      string   `json:"exceptionClass"`
	Message    string   `json:"message"`
	Parameters []string `json:"parameters,omitempty"`
}

// maybeSpringError is the error probably returned by the framework used by HawkBit.
type maybeSpringError struct {
	TimeStamp string `json:"timestamp"`
	Status    int    `json:"status"`
	ErrorType string `json:"error"`
	Message   string `json:"message"`
}

func (e *maybeSpringError) Error() string {
	// TODO: handle cases where message is non-empty.
	return e.ErrorType
}
