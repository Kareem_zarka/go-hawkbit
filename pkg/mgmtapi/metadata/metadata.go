// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package metadata contains type definitions for working with meta-data entries.
//
// HawkBit associates extensible meta-data coupled with individual entities
// like software modules, distribution sets and so on. At the most basic level
// metadata is a collection of key=value pairs. Some models contain other
// fields, for example the software module meta-data also contains information
// about visibility of a particular entry from a device point of view.
//
// The implementation is based on the official documentation at:
//
//   - https://www.eclipse.org/hawkbit/apis/mgmt/targets/
//   - https://www.eclipse.org/hawkbit/apis/mgmt/distributionsets/
//   - https://www.eclipse.org/hawkbit/apis/mgmt/softwaremodules/
package metadata

// PathSuffix is the suffix for URL path of all meta-data endpoints.
const PathSuffix = "/metadata"
