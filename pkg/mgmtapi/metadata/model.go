// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package metadata

import (
	"context"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Model is an URL to a collection of metadata of a particular entity.
//
// The type parameter EntryT is the concrete type of the meta-data entry.
type Model[EntryT any, EntryPtrT interface {
	*EntryT
	ContentType() string
}] string

// Ref returns the link to a meta-data entry with the given key.
func (m Model[EntryT, EntryPtrT]) Ref(key string) Ref[EntryT, EntryPtrT] {
	var b strings.Builder

	b.Grow(len(m) + 1 + len(key))
	b.WriteString(string(m))
	b.WriteByte('/')
	b.WriteString(key)

	return Ref[EntryT, EntryPtrT](b.String())
}

// Create creates one or more metadata entires and returns their data.
func (m Model[EntryT, EntryPtrT]) Create(ctx context.Context, cli *restapi.Client, res []EntryT) ([]EntryT, error) {
	v, err := restapi.CreateResources[EntryT, EntryT](ctx, cli, string(m), res)
	if err != nil {
		return nil, errprefix.NewError[restapi.CreateErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Find searches or enumerates metadata entries.
//
// FindOptions define ordering, search and the size and offset of the returned
// data set. Multiple calls to Find are usually required to traverse all
// results.
func (m Model[EntryT, EntryPtrT]) Find(ctx context.Context, cli *restapi.Client, opts restapi.FindOptions) (*restapi.Found[EntryT], error) {
	v, err := restapi.FindResources[EntryT](ctx, cli, string(m), opts)
	if err != nil {
		return nil, errprefix.NewError[restapi.FindErrorPrefix[Namer]](err)
	}

	return v, nil
}
