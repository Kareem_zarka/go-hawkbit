// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package mgmtapi contains Go bindings to the HawkBit Management APIs.
//
// Create a Client instance with a HawkBit instance URL and account
// credentials.  The client exposes access to top-level resource groups, each
// of which supports C-R-U-D style operations.
package mgmtapi
