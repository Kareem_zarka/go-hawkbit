// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package swmodtypes contains type definitions for working with Software Module Types.
//
// Software Module Type describes properties of a group of Software Modules. One
// interesting property is the maximum number of assignments. This allows
// constraining updates to guarantee, for example, that only one OS image is
// present in a given update payload.
//
// The implementation is based on the official documentation at
// https://www.eclipse.org/hawkbit/apis/mgmt/softwaremoduletypes/
package swmodtypes

import (
	"strconv"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements restapi.ResourceNamer for hawkbit software module type resource.
type Namer struct{}

// ResourceName returns the singular name of the type used by error prefix types.
func (Namer) ResourceName() string {
	return "software module type"
}

// ResourcePluralName returns the plural type name used by error prefix types.
func (Namer) ResourcePluralName() string {
	return "software module types"
}

// CreateData describes a software module type to create.
type CreateData struct {
	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Key            string `json:"key,omitempty"`
	MaxAssignments int    `json:"maxAssignments,omitempty"`
	// GUI shows a software/firmware flag and a color. Those are not editable.
	// Reported as https://github.com/eclipse/hawkbit/issues/1240
}

// ID is the primary key for software module types.
type ID int

// String returns the ID as a string.
func (id ID) String() string {
	return strconv.Itoa(int(id))
}

// Set sets the ID to the given string value.
//
// The value must be a decimal representation of a number.
func (id *ID) Set(s string) error {
	n, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*id = ID(n)

	return nil
}

// Data describes a retrieved or created software module type.
type Data struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Key            string `json:"key"`
	MaxAssignments int    `json:"maxAssignments"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`

	Links struct {
		Self restapi.Link `json:"self"`
	} `json:"_links,omitempty"`
}

// FindData describes a found software module type.
//
// FindData is usually a subset of Data.
type FindData struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	// Not documented for the response but realistically provided.
	Key            string `json:"key"`
	MaxAssignments int    `json:"maxAssignments"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`
}

// UpdateData describes updates to a software module type.
//
// Nil fields are not updated and can be left out.
type UpdateData struct {
	Description *string `json:"description,omitempty"`
}

func (up *UpdateData) ContentType() string {
	return contenttype.AppHalJson
}
