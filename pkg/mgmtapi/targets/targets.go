// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package targets contains type definitions for working with Targets.
//
// A target is a device that is managed by HawkBit. Each Target has an unique
// ID, also known as a controller ID. A target is related to DistributionSet
// which describes the software that is installed or is supposed to be
// installed.
//
// The implementation is based on the official documentation at
// https://www.eclipse.org/hawkbit/apis/mgmt/targets/
package targets

import (
	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements restapi.ResourceNamer for hawkbit target resource.
type Namer struct{}

// ResourceName returns the singular name of the type used by error prefix types.
func (Namer) ResourceName() string {
	return "target"
}

// ResourcePluralName returns the plural type name used by error prefix types.
func (Namer) ResourcePluralName() string {
	return "targets"
}

// ID is the primary key for targets.
type ID string

// String returns the ID as a string.
func (id ID) String() string {
	return string(id)
}

// Set implements the flag.Value interface.
func (id *ID) Set(s string) error {
	*id = ID(s)

	return nil
}

// CreateData describes a target to create.
type CreateData struct {
	// As an exception in the general trend in this API. Targets can be crated
	// with a pre-assigned primary key. The same key must be provisioned into
	// devices.
	ID ID `json:"controllerId"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Address       string `json:"address,omitempty"`
	SecurityToken string `json:"securityToken,omitempty"`
}

// Data describes a retrieved or created target.
type Data struct {
	ID ID `json:"controllerId"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Address                 string       `json:"address,omitempty"`
	IpAddress               string       `json:"ipAddress,omitempty"`
	SecurityToken           string       `json:"securityToken,omitempty"`
	UpdateStatus            UpdateStatus `json:"updateStatus,omitempty"`
	RequestAttrs            bool         `json:"requestAttributes,omitempty"`
	InstalledAt             int64        `json:"installedAt,omitempty"`
	LastControllerRequestAt int64        `json:"lastControllerRequestAt,omitempty"`
	PollStatus              struct {
		LastRequestAt         int64 `json:"lastRequestAt"`
		NextExpectedRequestAt int64 `json:"nextExpectedRequestAt"`
		Overdue               bool  `json:"overdue,omitempty"`
	} `json:"pollstatus"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	// Is Deleted here as well?

	Links struct {
		Self        restapi.Link `json:"self"`
		AssignedDS  restapi.Link `json:"assignedDS"`
		InstalledDS restapi.Link `json:"installedDS"`
		Attributes  restapi.Link `json:"attributes"`
		Actions     restapi.Link `json:"actions"`
		MetaData    restapi.Link `json:"metadata"`
	} `json:"_links,omitempty"`
}

// FindData describes a found target.
//
// FindData is usually a subset of Data.
type FindData struct {
	ID ID `json:"controllerId"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Address                 string       `json:"address,omitempty"`
	IpAddress               string       `json:"ipAddress,omitempty"`
	SecurityToken           string       `json:"securityToken,omitempty"`
	UpdateStatus            UpdateStatus `json:"updateStatus,omitempty"`
	RequestAttrs            bool         `json:"requestAttributes,omitempty"`
	InstalledAt             int64        `json:"installedAt,omitempty"`
	LastControllerRequestAt int64        `json:"lastControllerRequestAt,omitempty"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
}

// UpdateData describes updates to a target.
//
// Nil fields are not updated and can be left out.
type UpdateData struct {
	Name          *string `json:"name,omitempty"`
	Description   *string `json:"description,omitempty"`
	ID            *string `json:"controllerId,omitempty"`
	Address       *string `json:"address,omitempty"`
	SecurityToken *string `json:"securityToken,omitempty"`
	RequestAttrs  *bool   `json:"requestAttributes,omitempty"`
}

func (*UpdateData) ContentType() string {
	return contenttype.AppHalJson
}
