// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package targets

// UpdateStatus is the status of an update operation of a target.
type UpdateStatus string

const (
	StatusError      UpdateStatus = "error"
	StatusInSync     UpdateStatus = "in_sync"
	StatusPending    UpdateStatus = "pending"
	StatusRegistered UpdateStatus = "registered"
	StatusUnknown    UpdateStatus = "unknown"
)
