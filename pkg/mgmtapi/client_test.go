// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mgmtapi_test

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/targets"
)

func TestCreateTarget(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if user, pass, ok := req.BasicAuth(); !ok || user != "user" || pass != "pass" {
			t.Fatalf("incorrect authentication: user: %q, pass: %q", user, pass)
		}

		if path := req.URL.Path; path != "/rest/v1/targets" {
			t.Fatalf("unexpected request path %q", path)
		}
		if req.Method != "POST" {
			t.Fatalf("unexpected request method %v", req.Method)
		}
		if ct := req.Header.Get("Content-Type"); ct != "application/hal+json;charset=utf-8" {
			t.Fatalf("unexpected request content type %v", ct)
		}

		data, err := io.ReadAll(req.Body)
		if err != nil {
			t.Fatalf("cannot read request body: %v", err)
		}

		var reqData []map[string]any
		if err := json.Unmarshal(data, &reqData); err != nil {
			t.Fatalf("cannot unmarshal request body: %v", err)
		}

		expected := []map[string]any{{
			"controllerId":  "1",
			"name":          "dev-1",
			"description":   "first device",
			"address":       "http://potato",
			"securityToken": "secret",
		}}
		if !reflect.DeepEqual(reqData, expected) {
			t.Fatalf("unexpected request body: %q", reqData)
		}

		var toCreate []targets.CreateData

		if err := json.Unmarshal(data, &toCreate); err != nil {
			t.Fatalf("cannot unmarshal request body as target: %v", err)
		}

		w.Header().Add("Content-Type", "application/hal+json;charset=utf-8")
		w.WriteHeader(201)

		respData := []map[string]any{{
			"controllerId":  "1",
			"name":          "dev-1",
			"description":   "first device",
			"address":       "http://potato",
			"securityToken": "secret",
			"_links": map[string]any{
				"self": map[string]any{
					"href": req.RequestURI + "/1",
				},
			},
		}}
		data, err = json.Marshal(respData)
		if err != nil {
			t.Fatalf("cannot marshal response data: %v", err)
		}

		n, err := w.Write(data)
		if err != nil {
			t.Fatalf("cannot write response data: %v", err)
		}
		if n != len(data) {
			t.Fatalf("wrote only part of the data %d < %d", n, len(data))
		}
	}))
	defer srv.Close()

	cli := mgmtapi.NewClient(srv.URL, "user", "pass")

	created, err := cli.TargetModel().Create(context.TODO(), cli.Client, []targets.CreateData{{
		ID:            "1",
		Name:          "dev-1",
		Description:   "first device",
		Address:       "http://potato",
		SecurityToken: "secret",
	}})

	if err != nil {
		t.Fatalf("cannot create targets: %v", err)
	}

	if len(created) != 1 {
		t.Fatalf("unexpected number of created targets: %d", len(created))
	}

	tgt := created[0]

	if tgt.ID != "1" {
		t.Fatalf("unexpected ID of the created target: %v", tgt.ID)
	}

	if tgt.Name != "dev-1" {
		t.Fatalf("unexpected name of the created target: %v", tgt.Name)
	}

	if tgt.Description != "first device" {
		t.Fatalf("unexpected description of the created target: %v", tgt.Description)
	}

	if tgt.Address != "http://potato" {
		t.Fatalf("unexpected address of the created target: %v", tgt.Address)
	}

	if tgt.SecurityToken != "secret" {
		t.Fatalf("unexpected security token of the created target: %v", tgt.SecurityToken)
	}

	if tgt.Links.Self.Href != "/rest/v1/targets/1" {
		t.Fatalf("unexpected self href of the created target: %v", tgt.Links.Self.Href)
	}
}

func TestDeleteTarget(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if user, pass, ok := req.BasicAuth(); !ok || user != "user" || pass != "pass" {
			t.Fatalf("incorrect authentication: user: %q, pass: %q", user, pass)
		}
		if path := req.URL.Path; path != "/rest/v1/targets/1" {
			t.Fatalf("unexpected request path %q", path)
		}
		if req.Method != "DELETE" {
			t.Fatalf("unexpected request method %v", req.Method)
		}

		if req.ContentLength != 0 {
			t.Fatalf("unexpected content length: %d", req.ContentLength)
		}

		w.Header().Add("Content-Type", "application/hal+json;charset=utf-8")
		w.WriteHeader(200)
	}))
	defer srv.Close()

	cli := mgmtapi.NewClient(srv.URL, "user", "pass")

	err := cli.TargetModel().Ref("1").Delete(context.TODO(), cli.Client)
	if err != nil {
		t.Fatalf("cannot delete target: %v", err)
	}
}
