// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package artifacts

import (
	"context"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Model is an URL to a collection of all the artifacts of a specific software module.
type Model string

// Ref returns the link to an artifact a given ID.
func (m Model) Ref(id ID) Ref {
	var b strings.Builder

	idStr := id.String()

	b.Grow(len(m) + 1 + len(idStr))
	b.WriteString(string(m))
	b.WriteByte('/')
	b.WriteString(idStr)

	return Ref(b.String())
}

// GetAll returns all the artifacts published to this model.
//
// This API is documented at
// https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Software-Modules/operation/getArtifacts
func (m Model) GetAll(ctx context.Context, cli *restapi.Client) ([]Data, error) {
	v, err := restapi.ReadResourceSlice[Data](ctx, cli, string(m))
	if err != nil {
		return nil, errprefix.NewError[restapi.FindErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Upload uploads an artifact.
//
// This API is documented at
// https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Software-Modules/operation/uploadArtifact
func (m Model) Upload(ctx context.Context, cli *restapi.Client, path string) (*Data, error) {
	v, err := restapi.Upload[Data](ctx, cli, string(m), path)

	if err != nil {
		return nil, errprefix.NewError[restapi.CreateErrorPrefix[Namer]](err)
	}

	return v, nil
}
