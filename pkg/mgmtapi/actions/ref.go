// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package actions

import (
	"context"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/actionstatuses"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Ref is an URL to a specific action.
//
// Ref encodes TargetID and ActionID.
type Ref string

// StatusModel returns the model of action status entries of this action.
func (r Ref) StatusModel() actionstatuses.Model {
	return actionstatuses.Model(r + "/status")
}

// Get retrieves action data.
//
// This API is documented at https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Actions/operation/getAction_1
func (r Ref) Get(ctx context.Context, cli *restapi.Client) (*Data, error) {
	v, err := restapi.ReadResource[Data](ctx, cli, string(r))
	if err != nil {
		return nil, errprefix.NewError[restapi.ReadErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Update updates an action.
//
// At present the only operation is to switch from "soft" to "forced" action force type.
//
// This API is documented at https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Targets/operation/updateAction
func (r Ref) Update(ctx context.Context, cli *restapi.Client, up *UpdateData) (*Data, error) {
	v, err := restapi.UpdateResource[UpdateData, Data, *UpdateData](ctx, cli, string(r), up)
	if err != nil {
		return nil, errprefix.NewError[restapi.UpdateErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Cancel cancels an action.
//
// The target device will see a cancellation request and will be able to send feedback
// indication if the cancellation was successful or not.
//
// This API is documented at https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Targets/operation/cancelAction
func (r Ref) Cancel(ctx context.Context, cli *restapi.Client) error {
	if err := restapi.DeleteResource(ctx, cli, string(r)); err != nil {
		return errprefix.NewError[restapi.DeleteErrorPrefix[Namer]](err)
	}

	return nil
}

// ForceCancel force-cancels an action.
//
// ForceCancel is similar to Cancel but does not involve the device having
// a say in the matter. It can be used to unbreak the state machine but should not be
// used in normal circumstances.
//
// This API is documented at https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Targets/operation/cancelAction
func (r Ref) ForceCancel(ctx context.Context, cli *restapi.Client) error {
	href := string(r) + "?force=true"
	if err := restapi.DeleteResource(ctx, cli, href); err != nil {
		return errprefix.NewError[restapi.DeleteErrorPrefix[Namer]](err)
	}

	return nil
}
