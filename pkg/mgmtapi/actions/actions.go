// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package actions contains type definitions for working with actions.
//
// An action is an operation requested by HawkBit and executed by a target device.
// Actions are visible in two collections, in the global collection for a given tenant
// and in the target-specific collection.
//
// Each action has a related "action status" model that contains all the
// updates to the action that were either generated internally or reported
// by the target device.
//
// The implementation is based on the official documentation at
// https://eclipse.dev/hawkbit/rest-api/mgmt.html#tag/Actions
package actions

import (
	"strconv"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements ResourceNamer for actions.
type Namer struct{}

func (Namer) ResourceName() string {
	return "action"
}

func (Namer) ResourcePluralName() string {
	return "actions"
}

// ID is the primary key for actions.
type ID int

// String implements the fmt.Stringer and flag.Value interfaces.
func (id ID) String() string {
	return strconv.Itoa(int(id))
}

// Set sets the ID to the given string value.
//
// The value must be a decimal representation of a number.
//
// Set implements the flag.Value interface.
func (id *ID) Set(s string) error {
	// TODO: de-duplicate all integer ID types while maintaining type safety.
	n, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*id = ID(n)

	return nil
}

// MaintenanceWindow describes a maintenance window for an action.
type MaintenanceWindow struct {
	Schedule    string `json:"schedule"`    // TODO: add type for quartz cron schedule notation (0 15 10 * *).
	Duration    string `json:"duration"`    // String in format "HH:MM:SS"
	TimeZone    string `json:"timezone"`    // TODO: add type for time zone notation (+/-HH:MM)
	NextStartAt int64  `json:"nextStartAt"` // Milliseconds since epoch UTC.
}

// FindData describes a found action.
//
// FindData is usually a subset of Data.
//
// TODO: there are two representations: compact and full.
// Full seems identical to Data, with populated links section.
// This is not modelled in the type system yet.
type FindData struct {
	ID                ID                `json:"id"`
	Type              Type              `json:"type"`
	Status            Status            `json:"status"`
	DetailStatus      DetailStatus      `json:"detailStatus"`
	Weight            int               `json:"weight"`
	MaintenanceWindow MaintenanceWindow `json:"maintenanceWindow,omitempty"`
	ForceType         string            `json:"forceType,omitempty"`
	RolloutID         int               `json:"rollout,omitempty"`
	RolloutName       string            `json:"rolloutName,omitempty"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"` // Milliseconds since epoch UTC.
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"` // Milliseconds since epoch UTC.

	Links struct {
		Self restapi.Link `json:"self"` // TODO: indicate that the link is a reference to an actions.Data.
	} `json:"_links,omitempty"`
}

// Data describes a retrieved action.
type Data struct {
	ID                ID                `json:"id"`
	Type              Type              `json:"type"`
	Status            Status            `json:"status"`
	DetailStatus      DetailStatus      `json:"detailStatus"`
	Weight            int               `json:"weight"`
	MaintenanceWindow MaintenanceWindow `json:"maintenanceWindow,omitempty"`
	ForceType         string            `json:"forceType,omitempty"`
	RolloutID         int               `json:"rollout,omitempty"`
	RolloutName       string            `json:"rolloutName,omitempty"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"` // Milliseconds since epoch UTC.
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"` // Milliseconds since epoch UTC.

	Links struct {
		Self   restapi.Link `json:"self"`   // TODO: indicate that the link is a reference to an actions.Data.
		Target restapi.Link `json:"target"` // TODO: indicate that the link is a reference to a target.
		// TODO: Target has an embedded "name" attribute.
		DistributionSet restapi.Link `json:"distributionSet"` // TODO: indicate that the link is a reference to a distribution set.
		// TODO: DistributionSet has an embedded "name" attribute.
		Status  restapi.Link `json:"status"`  // TODO: ???
		Rollout restapi.Link `json:"rollout"` // TODO: indicate that the link is a reference to a rollout.
		// TODO: Rollout has an embedded "name" attribute.
	} `json:"_links,omitempty"`
}

type UpdateData struct {
	ForceType *string `json:"forceType"`
}

func (*UpdateData) ContentType() string {
	return contenttype.AppHalJson
}

// Type describes the type of an action.
type Type string

const (
	// TypeUpdate is a type of action that updates software on a target device.
	TypeUpdate Type = "update"
	// TypeCancel is a type of action that cancels another action.
	TypeCancel Type = "cancel"
)

// ForceType describes the mode of performing an action.
type ForceType string

const (
	// ForceTypeSoft is used to attempt an action.
	ForceTypeSoft ForceType = "soft"
	// ForceTypeForce is used to force an action.
	ForceTypeForced ForceType = "forced"
	// ForceTypeTimeForce is is an action that starts "soft" and becomes "forced" after a certain time.
	ForceTypeTimeForced ForceType = "timeforced"
	// ForceTypeDownloadOnly is used to download the update but not install it.
	ForceTypeDownloadOnly ForceType = "downloadonly"
)

// Status describes the status of an action.
type Status string

const (
	// StatusFinished indicates that the action has been finished.
	StatusFinished Status = "finished"
	// StatusRunning indicates that the action is currently running.
	StatusPending Status = "pending"
)

// DetailStatus describes the status of an action in more detail.
type DetailStatus string

const (
	DetailStatusRetrieved      DetailStatus = "retrieved"
	DetailStatusFinished       DetailStatus = "finished"
	DetailStatusError          DetailStatus = "error"
	DetailStatusRunning        DetailStatus = "running"
	DetailStatusWarning        DetailStatus = "warning"
	DetailStatusCanceling      DetailStatus = "canceling"
	DetailStatusCanceled       DetailStatus = "canceled"
	DetailStatusCancelRejected DetailStatus = "cancel_rejected"
	DetailStatusDownload       DetailStatus = "download"
	DetailStatusDownloaded     DetailStatus = "downloaded"
)
