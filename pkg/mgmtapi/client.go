// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mgmtapi

import (
	"net/url"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/actions"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dses"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dstypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmodtypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/targets"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Client allows connecting to Hawkbit management APIs
type Client struct {
	*restapi.Client

	url *url.URL
}

// NewClient creates a new client with the given instance URL and user credentials.
func NewClient(addr, username, password string) *Client {
	u, err := url.Parse(addr)
	if err != nil {
		panic(err)
	}

	return &Client{
		Client: restapi.NewClient(username, password),
		url:    u,
	}
}

// NewTenantClient creates a new client with the given instance URL and a tenant-specific user credentials.
func NewTenantClient(addr, tenant, username, password string) *Client {
	u, err := url.Parse(addr)
	if err != nil {
		panic(err)
	}

	return &Client{
		Client: restapi.NewTenantClient(tenant, username, password),
		url:    u,
	}
}

func mustParseURL(rawURL string) *url.URL {
	u, err := url.Parse(rawURL)
	if err != nil {
		panic(err)
	}

	return u
}

var (
	targetsURL    = mustParseURL("rest/v1/targets")
	swmodtypesURL = mustParseURL("rest/v1/softwaremoduletypes")
	swmodsURL     = mustParseURL("rest/v1/softwaremodules")
	dstypesURL    = mustParseURL("rest/v1/distributionsettypes")
	dsesURL       = mustParseURL("rest/v1/distributionsets")
	actionsURL    = mustParseURL("rest/v1/actions")
)

// TargetModel returns the model for targets.
func (cli *Client) TargetModel() targets.Model {
	return targets.Model(cli.url.ResolveReference(targetsURL).String())
}

// SoftwareModuleTypeModel returns the model for software module types.
func (cli *Client) SoftwareModuleTypeModel() swmodtypes.Model {
	return swmodtypes.Model(cli.url.ResolveReference(swmodtypesURL).String())
}

// SoftwareModuleModel returns a model of all software module.
func (cli *Client) SoftwareModuleModel() swmods.Model {
	return swmods.Model(cli.url.ResolveReference(swmodsURL).String())
}

// DistributionSetTypes returns a model of all distribution set types.
func (cli *Client) DistributionSetTypeModel() dstypes.Model {
	return dstypes.Model(cli.url.ResolveReference(dstypesURL).String())
}

// DistributionSets returns a group of all distribution sets.
func (cli *Client) DistributionSetModel() dses.Model {
	return dses.Model(cli.url.ResolveReference(dsesURL).String())
}

// ActionModel returns a group of all actions.
func (cli *Client) ActionModel() actions.Model {
	return actions.Model(cli.url.ResolveReference(actionsURL).String())
}
