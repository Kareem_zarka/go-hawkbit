// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package swmods

import (
	"context"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Model is an URL to a collection of all the software modules.
type Model string

// Ref returns the link to a software module with a given ID.
func (m Model) Ref(id ID) Ref {
	var b strings.Builder

	idStr := id.String()

	b.Grow(len(m) + 1 + len(idStr))
	b.WriteString(string(m))
	b.WriteByte('/')
	b.WriteString(idStr)

	return Ref(b.String())
}

// Create creates one or more software modules and returns their data.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/softwaremodules-api-guide/#_post_rest_v1_softwaremodules
func (m Model) Create(ctx context.Context, cli *restapi.Client, res []CreateData) ([]Data, error) {
	v, err := restapi.CreateResources[CreateData, Data](ctx, cli, string(m), res)
	if err != nil {
		return nil, errprefix.NewError[restapi.CreateErrorPrefix[Namer]](err)
	}

	return v, nil
}

// Find searches or enumerates software modules.
//
// FindOptions define ordering, search and the size and offset of the returned data set.
// Multiple calls to Find are usually required to traverse all results.
//
// This API is documented at https://www.eclipse.org/hawkbit/rest-api/softwaremodules-api-guide/#_get_rest_v1_softwaremodules
func (m Model) Find(ctx context.Context, cli *restapi.Client, opts restapi.FindOptions) (*restapi.Found[FindData], error) {
	v, err := restapi.FindResources[FindData](ctx, cli, string(m), opts)
	if err != nil {
		return nil, errprefix.NewError[restapi.FindErrorPrefix[Namer]](err)
	}

	return v, nil
}
