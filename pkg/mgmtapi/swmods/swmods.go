// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package swmodws contains type definitions for working with Software Modules.
//
// SoftwareModule represents a piece of software at a specific version.
//
// HawkBit associates binary artifacts with specific software modules. A
// software module refers to a SoftwareModuleType with a relationship on the
// Type field on the local side and on the Key field on the remote side.
//
// The implementation is based on the official documentation at
// https://www.eclipse.org/hawkbit/apis/mgmt/softwaremodules/
package swmods

import (
	"strconv"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements ResourceNamer for hawkbit Software Modules.
type Namer struct{}

// ResourceName returns the singular name of the type used by error prefix types.
func (Namer) ResourceName() string {
	return "software module"
}

// ResourcePluralName returns the plural type name used by error prefix types.
func (Namer) ResourcePluralName() string {
	return "software modules"
}

// CreateData describes a software module to create.
type CreateData struct {
	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Vendor  string `json:"vendor"`
	Version string `json:"version"`
	Type    string `json:"type"` // References the key of software module type.
}

// ID is the primary key for software modules.
type ID int

// String returns the ID as a string.
func (id ID) String() string {
	return strconv.Itoa(int(id))
}

// Set sets the ID to the given string value.
//
// The value must be a decimal representation of a number.
func (id *ID) Set(s string) error {
	n, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*id = ID(n)

	return nil
}

// Data describes a retrieved or created software module.
type Data struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Vendor  string `json:"vendor"`
	Version string `json:"version"`
	Type    string `json:"type"` // References the key of software module type.

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`

	Links struct {
		Self      restapi.Link `json:"self"`
		Type      restapi.Link `json:"type"`
		Artifacts restapi.Link `json:"artifacts"`
		MetaData  restapi.Link `json:"metadata"`
	} `json:"_links,omitempty"`
}

// AlternateID returns the unique "name:version" pair.
//
// This method exists as a work-around for https://github.com/golang/go/issues/48522
// and should be removed once Go can access structure fields from generic functions.
func (d Data) AlternateID() string {
	return d.Name + ":" + d.Version
}

// FindData describes a found software module.
//
// FindData is usually a subset of Data.
type FindData struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Vendor  string `json:"vendor"`
	Version string `json:"version"`
	Type    string `json:"type"` // References the key of software module type.

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`

	Links struct {
		Self restapi.Link `json:"self"`
	} `json:"_links,omitempty"`
}

// PrimaryID returns the ID.
//
// This method exists as a work-around for https://github.com/golang/go/issues/48522
// and should be removed once Go can access structure fields from generic functions.
func (d FindData) PrimaryID() ID {
	return d.ID
}

// UpdateData describes updates to a software module.
//
// Nil fields are not updated and can be left out.
type UpdateData struct {
	Description *string `json:"description,omitempty"`
	Vendor      *string `json:"vendor"`
}

func (*UpdateData) ContentType() string {
	return contenttype.AppHalJson
}
