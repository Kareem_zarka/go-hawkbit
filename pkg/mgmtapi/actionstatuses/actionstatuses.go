// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package actionstatues contains type definitions for working with action status entries.
//
// Entries have internal IDs but are not directly addressable.
// The only way to list them is to use the Find method on the action status model,
// which can be retrieved from an actions.Ref value.
package actionstatuses

// Namer implements ResourceNamer for action statuses.
type Namer struct{}

func (Namer) ResourceName() string {
	return "action status"
}

func (Namer) ResourcePluralName() string {
	return "action statuses"
}

// ID is the primary key for action status entries.
type ID int

// FindData describes a single status entry of an action.
type FindData struct {
	ID         ID       `json:"id"`
	Type       Type     `json:"type"`
	Messages   []string `json:"messages"`
	Code       int      `json:"code,omitempty"`
	ReportedAt int64    `json:"reportedAt"` // Milliseconds since epoch UTC.
}

// Type describes the type of status entry.
type Type string

const (
	// TypeRetrieved indicates that the action has been retrieved by the
	// target. It is sufficient to GET the DDI base resource URL for HawkBit to
	// generate an entry with this status type automatically.
	TypeRetrieved           Type = "retrieved"
	TypeRunning             Type = "running"
	TypeError               Type = "error"
	TypeWarning             Type = "warning"
	TypeFinished            Type = "finished"
	TypeCanceled            Type = "canceled"
	TypeCanceling           Type = "canceling"
	TypeCancelRejected      Type = "cancel_rejected"
	TypeDownload            Type = "download"
	TypeScheduled           Type = "scheduled"
	TypeDownloaded          Type = "downloaded"
	TypeWaitForConfirmation Type = "wait_for_confirmation"
)
