// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package dstypes contains type definitions for working with Distribution Set Types.
//
// Distribution set type acts as a schema for valid distributions by defining
// the set of mandatory and optional software modules types.
//
// The implementation is based on the official documentation at
// https://www.eclipse.org/hawkbit/apis/mgmt/distributionsettypes/
package dstypes

import (
	"strconv"

	"gitlab.com/zygoon/go-hawkbit/pkg/contenttype"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmodtypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

// Namer implements ResourceNamer for distribution set types.
type Namer struct{}

// ResourceName returns the singular name of the type used by error prefix types.
func (Namer) ResourceName() string {
	return "distribution set type"
}

// ResourcePluralName returns the plural type name used by error prefix types.
func (Namer) ResourcePluralName() string {
	return "distribution set types"
}

// CreateData describes a distribution set type to create.
type CreateData struct {
	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Key string `json:"key"`

	OptionalModules  []restapi.IdRef[swmodtypes.ID] `json:"optionalmodules,omitempty"`
	MandatoryModules []restapi.IdRef[swmodtypes.ID] `json:"mandatorymodules,omitempty"`
}

// ID is the primary key for distribution set types.
type ID int

// String returns the ID as a string.
func (id ID) String() string {
	return strconv.Itoa(int(id))
}

// Set sets the ID to the given string value.
//
// The value must be a decimal representation of a number.
func (id *ID) Set(s string) error {
	n, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*id = ID(n)

	return nil
}

// Data describes a retrieved or created distribution set type.
type Data struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Key string `json:"key"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`

	Links struct {
		Self             restapi.Link `json:"self,omitempty"`
		MandatoryModules restapi.Link `json:"mandatorymodules,omitempty"`
		OptionalModules  restapi.Link `json:"optionalmodules,omitempty"`
	} `json:"_links,omitempty"`
}

// OptionalModules returns the relation to optional software module types.
func (d *Data) OptionalModules() swmodtypes.ManyRelation {
	return swmodtypes.ManyRelation(d.Links.OptionalModules.Href)
}

// MandatoryModules returns the relation to mandatory software module types.
func (d *Data) MandatoryModules() swmodtypes.ManyRelation {
	return swmodtypes.ManyRelation(d.Links.MandatoryModules.Href)
}

// FindData describes a found distribution set type.
//
// FindData is usually a subset of Data.
type FindData struct {
	ID ID `json:"id"`

	Name        string `json:"name"`
	Description string `json:"description,omitempty"`

	Key string `json:"key"`

	CreatedBy      string `json:"createdBy,omitempty"`
	CreatedAt      int64  `json:"createdAt,omitempty"`
	LastModifiedBy string `json:"lastModifiedBy,omitempty"`
	LastModifiedAt int64  `json:"lastModifiedAt,omitempty"`
	Deleted        bool   `json:"deleted,omitempty"`
}

// UpdateData describes updates to a distribution set type.
//
// Nil fields are not updated and can be left out.
type UpdateData struct {
	Description *string `json:"description,omitempty"`
}

func (*UpdateData) ContentType() string {
	return contenttype.AppHalJson
}
