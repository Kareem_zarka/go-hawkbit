// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package errprefix provides generic prefixed error type.
package errprefix

// ErrorPrefixer is a type constraint providing prefix to wrapped errors.
type ErrorPrefixer interface {
	// ErrorPrefix returns a string which is prefixed to the wrapped error.
	//
	// The returned string should provide information about the context external
	// to the error. The returned string will be combined with a colon, space
	// and the error message of the wrapped error.
	//
	// This method is always called on the zero value of the type implementing
	// it. If you need to access runtime information, do not use StaticPrefixer.
	ErrorPrefix() string
}

// Error combines a typed prefix with a runtime error.
//
// Error helps to provide context to an error, without involving stack traces
// or other internals which may be undesirable.
//
// If P is a public type, the concrete error type is equally public.
type Error[P ErrorPrefixer] struct {
	Err error
}

// NewError wraps an error with a typed prefix.
//
// Unlike fmt.Errorf("prefix: %w") the return value has a specific type, making
// it compatible with type assertions, errors.Is and errors.As.
func NewError[P ErrorPrefixer](err error) Error[P] {
	return Error[P]{Err: err}
}

// Error returns the prefixed error message.
func (e Error[P]) Error() string {
	var zero P

	return zero.ErrorPrefix() + ": " + e.Err.Error()
}

// Unwrap returns the wrapped error.
func (e Error[P]) Unwrap() error {
	return e.Err
}
