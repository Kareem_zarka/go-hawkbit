// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package errprefix_test

import (
	"errors"
	"testing"

	"gitlab.com/zygoon/go-hawkbit/pkg/errprefix"
)

type testPrefix struct{}

func (p testPrefix) ErrorPrefix() string {
	return "prefix"
}

func TestTestPrefix(t *testing.T) {
	tp := testPrefix{}

	if p := tp.ErrorPrefix(); p != "prefix" {
		t.Fatalf("unexpected error prefix: %q", p)
	}
}

func TestWrapOf(t *testing.T) {
	// Note that errors.New returns unique and different values even though the text is identical.
	inner := errors.New("msg")
	err := errprefix.NewError[testPrefix](inner)

	if msg := err.Error(); msg != "prefix: msg" {
		t.Fatalf("unexpected error message from prefix error: %q", msg)
	}

	if e := errors.Unwrap(err); e == nil || e.Error() != inner.Error() {
		t.Fatalf("errors.Unwrap returned unexpected value: %v", e)
	}

	if !errors.Is(err, inner) {
		t.Fatalf("errors.Is did not match the inner value (msg)")
	}

	if !errors.Is(err, errprefix.NewError[testPrefix](inner)) {
		t.Fatalf("errors.Is did not match the outer value")
	}
}
