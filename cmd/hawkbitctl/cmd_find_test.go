// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main_test

import (
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"
	hawkbitctl "gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dses"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"

	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/adapters"
)

func TestOrderingValue(t *testing.T) {
	t.Run("nil-ptr", func(t *testing.T) {
		// This is required by the flag.Value interface.
		var zero hawkbitctl.OrderingValue

		if zero.String() != "" {
			t.Fatal("Zero value of OrderingValue must not panic and must produce an empty string")
		}
	})

	t.Run("set", func(t *testing.T) {
		var ord restapi.Ordering

		err := hawkbitctl.OrderingValue{OrderingPtr: &ord}.Set("f1,+f2,-f3")
		if err != nil {
			t.Fatal(err)
		}

		if !reflect.DeepEqual(ord, restapi.Ordering{
			{Field: "f1"},
			{Field: "f2", Ascending: true},
			{Field: "f3"},
		}) {
			t.Fatalf("Unexpected ordering: %#v", ord)
		}
	})

	t.Run("set-known", func(t *testing.T) {
		var ord restapi.Ordering

		known := hawkbitctl.NewStringSet([]string{"f1", "f2", "f3"})
		val := hawkbitctl.OrderingValue{OrderingPtr: &ord, KnownFields: known}

		if err := val.Set("f1,+f2,-f3"); err != nil {
			t.Fatal(err)
		}

		if !reflect.DeepEqual(ord, restapi.Ordering{
			{Field: "f1"},
			{Field: "f2", Ascending: true},
			{Field: "f3"},
		}) {
			t.Fatalf("Unexpected ordering: %#v", ord)
		}
	})

	t.Run("set-unknown", func(t *testing.T) {
		var ord restapi.Ordering

		known := hawkbitctl.NewStringSet([]string{"f1", "f2", "f3"})
		val := hawkbitctl.OrderingValue{OrderingPtr: &ord, KnownFields: known}

		err := val.Set("f4")
		if err == nil {
			t.Fatalf("Unexpected success")
		}

		if err.Error() != "unknown field \"f4\", must be one of f1,f2,f3" {
			t.Fatalf("Unexpected error: %v", err)
		}
	})

	t.Run("string", func(t *testing.T) {
		ord := restapi.Ordering{
			{Field: "f1"},
			{Field: "f2", Ascending: true},
			{Field: "f3"},
		}

		s := hawkbitctl.OrderingValue{OrderingPtr: &ord}.String()
		if s != "-f1,+f2,-f3" {
			t.Fatalf("Unexpected ordering: %#v", s)
		}
	})
}

func TestFindArgs(t *testing.T) {
	cmd := hawkbitctl.NewFindItemsCmd[dses.FindData](adapters.DistributionSet{})
	inv := cmdtest.Invoke(cmd, "potato")

	if err := inv.ExpectExitCode(1); err != nil {
		t.Log(err)
		t.Fail()
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Log(err)
		t.Fail()
	}

	if err := inv.ExpectStderr("hawkbitctl error: unexpected argument, try passing --help\n"); err != nil {
		t.Log(err)
		t.Fail()
	}
}
