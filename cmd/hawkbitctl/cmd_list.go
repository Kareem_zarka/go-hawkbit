// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type listAdapter[dataT any, cmdStateT any] interface {
	PluralName() string

	AddGetAllFlags(*flag.FlagSet, *cmdStateT)
	CanGetAll(*cmdStateT) error
	GetAll(context.Context, *mgmtapi.Client, *cmdStateT) ([]dataT, error)

	ListHeader(io.Writer) error
	ListItem(io.Writer, *dataT) error
}

// listItemsCmd provides a command for listing items of a given type.
type listItemsCmd[dataT any, cmdStateT any, adapterT listAdapter[dataT, cmdStateT]] struct {
	adapter adapterT
}

// NewListItemsCmd returns a new list items command with the given adapter.
func NewListItemsCmd[dataT any, cmdStateT any, adapterT listAdapter[dataT, cmdStateT]](a adapterT) cmdr.Cmd {
	return &listItemsCmd[dataT, cmdStateT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd listItemsCmd[dataT, cmdStateT, adapterT]) OneLiner() string {
	return fmt.Sprintf("list properties of many %s", cmd.adapter.PluralName())
}

func (cmd listItemsCmd[dataT, cmdStateT, adapterT]) Run(ctx context.Context, args []string) error {
	var cmdState cmdStateT

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)

	_, stdout, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	cmd.adapter.AddGetAllFlags(fs, &cmdState)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := cmd.adapter.CanGetAll(&cmdState); err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	w := tabwriter.NewWriter(stdout, 4, 4, 1, ' ', 0)

	if err := cmd.adapter.ListHeader(w); err != nil {
		return err
	}

	all, err := cmd.adapter.GetAll(ctx, cli, &cmdState)
	if err != nil {
		return err
	}

	for i := range all {
		if err := cmd.adapter.ListItem(w, &all[i]); err != nil {
			return err
		}
	}

	return w.Flush()
}
