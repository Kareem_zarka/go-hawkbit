// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main_test

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"

	hawkbitctl "gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl"
	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/adapters"
	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/mappers"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/artifacts"
)

func TestUploadArgs(t *testing.T) {
	loadClient := func(context.Context) (*mgmtapi.Client, error) {
		return nil, fmt.Errorf("this test doesn't load a hawkbit client")
	}
	ctx := context.TODO()
	arts := adapters.Artifact{
		SwModMapper:    mappers.NewSoftwareModuleNameVersionMapper(ctx, loadClient),
		ArtifactMapper: mappers.NewArtifactNameMapper[*adapters.ArtifactSelector](ctx, loadClient),
	}
	cmd := hawkbitctl.NewUploadCmd[artifacts.Data, adapters.ArtifactUploadState](arts)
	inv := cmdtest.Invoke(cmd, "potato")

	if err := inv.ExpectExitCode(1); err != nil {
		t.Log(err)
		t.Fail()
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Log(err)
		t.Fail()
	}

	if err := inv.ExpectStderr("hawkbitctl error: unexpected argument, try passing --help\n"); err != nil {
		t.Log(err)
		t.Fail()
	}
}
