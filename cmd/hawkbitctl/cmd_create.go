// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type createAdapter[createT, readT any] interface {
	SingularName() string

	AddCreateFlags(*flag.FlagSet, *createT)
	CanCreate(*createT) error
	Create(context.Context, *mgmtapi.Client, []createT) ([]readT, error)
	Show(context.Context, *mgmtapi.Client, *readT) error
}

// createItemCmd provides a command for creating items of a given type.
type createItemCmd[createT any, readT any, adapterT createAdapter[createT, readT]] struct {
	adapter adapterT
}

// NewCreateItemCmd returns a new create item command with the given adapter.
func NewCreateItemCmd[createT any, readT any, adapterT createAdapter[createT, readT]](a adapterT) cmdr.Cmd {
	return &createItemCmd[createT, readT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd createItemCmd[createT, readT, Adapter]) OneLiner() string {
	return fmt.Sprintf("create a new %s", cmd.adapter.SingularName())
}

// Run creates a new item through the HawkBit API.
func (cmd createItemCmd[createT, readT, adapterT]) Run(ctx context.Context, args []string) error {
	var it createT

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	cmd.adapter.AddCreateFlags(fs, &it)

	_, _, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := cmd.adapter.CanCreate(&it); err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	items, err := cmd.adapter.Create(ctx, cli, []createT{it})
	if err != nil {
		return err
	}

	for i := range items {
		if err := cmd.adapter.Show(ctx, cli, &items[i]); err != nil {
			return err
		}
	}

	return nil
}
