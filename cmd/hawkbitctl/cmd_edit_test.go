// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main_test

import (
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"

	hawkbitctl "gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl"
	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/adapters"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dses"
)

func TestEditArgs(t *testing.T) {
	cmd := hawkbitctl.NewEditItemCmd[dses.Data, dses.UpdateData, dses.ID](adapters.DistributionSet{})
	inv := cmdtest.Invoke(cmd, "potato")

	if err := inv.ExpectExitCode(1); err != nil {
		t.Log(err)
		t.Fail()
	}

	if err := inv.ExpectStdout(""); err != nil {
		t.Log(err)
		t.Fail()
	}

	if err := inv.ExpectStderr("hawkbitctl error: unexpected argument, try passing --help\n"); err != nil {
		t.Log(err)
		t.Fail()
	}
}
