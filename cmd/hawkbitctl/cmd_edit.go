// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type editAdapter[readT, updateT any, selectorT any] interface {
	SingularName() string

	AddSelectFlags(*flag.FlagSet, *selectorT)
	AddUpdateFlags(*flag.FlagSet, *updateT)
	CanSelect(selectorT) error
	Update(context.Context, *mgmtapi.Client, selectorT, *updateT) (*readT, error)

	Show(context.Context, *mgmtapi.Client, *readT) error
}

// editItemCmd provides a command for editing an item of a given type.
type editItemCmd[readT, updateT any, selectorT any, adapterT editAdapter[readT, updateT, selectorT]] struct {
	adapter adapterT
}

// NewEditItemCmd returns a new edit item command with the given adapter.
func NewEditItemCmd[readT, updateT any, selectorT any, adapterT editAdapter[readT, updateT, selectorT]](a adapterT) cmdr.Cmd {
	return &editItemCmd[readT, updateT, selectorT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd editItemCmd[readT, updateT, selectorT, adapterT]) OneLiner() string {
	var a adapterT

	return fmt.Sprintf("edit a specific %s", a.SingularName())
}

func (cmd editItemCmd[readT, updateT, selectorT, adapterT]) Run(ctx context.Context, args []string) error {
	var sel selectorT

	var up updateT

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	cmd.adapter.AddSelectFlags(fs, &sel)
	cmd.adapter.AddUpdateFlags(fs, &up)

	_, _, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := cmd.adapter.CanSelect(sel); err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	it, err := cmd.adapter.Update(ctx, cli, sel, &up)
	if err != nil {
		return err
	}

	return cmd.adapter.Show(ctx, cli, it)
}
