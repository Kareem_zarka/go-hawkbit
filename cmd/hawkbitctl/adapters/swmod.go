// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

const (
	swModIdHelp   = "Software module ID"
	cmdNameSwMod  = "software-module"
	cmdNameSwMods = "software-modules"
)

var (
	errSwModIdRequired      = errors.New("software module ID is required")
	errSwModNameRequired    = errors.New("software module name is required")
	errSwModVersionRequired = errors.New("software module version is required")
	errSwModTypeRequired    = errors.New("software module type is required")
)

// SoftwareModule is the hawkbitctl adapter for mgmtapi.SoftwareModule type.
type SoftwareModule struct {
	SwModMapper IdMapper[swmods.ID, string, struct{}]
}

func (SoftwareModule) SingularName() string {
	return cmdNameSwMod
}

func (SoftwareModule) PluralName() string {
	return cmdNameSwMods
}

func (SoftwareModule) KnownFields() []string {
	return []string{
		jsID,
		jsName,
		jsVersion,
		jsType,
		jsVersion,
		jsDescription,

		jsCreatedAt,
		jsCreatedBy,
		jsLastModifiedAt,
		jsLastModifiedBy,
		jsDeleted,
	}
}

func (SoftwareModule) AddCreateFlags(fs *flag.FlagSet, c *swmods.CreateData) {
	fs.StringVar(&c.Name, argName, "", "Name of the new software module")
	fs.StringVar(&c.Description, argDescription, "", "Description of the new software module")

	fs.StringVar(&c.Type, argType, "", "Reference to the key of an existing software module type")
	fs.StringVar(&c.Vendor, argVendor, "", "Vendor of the new software module")
	fs.StringVar(&c.Version, argVersion, "", "Version of the new software module")
}

func (cmd SoftwareModule) AddSelectFlags(fs *flag.FlagSet, id *swmods.ID) {
	fs.Var(IdValue[swmods.ID, *swmods.ID, string, struct{}]{
		IdPtr:  id,
		Mapper: cmd.SwModMapper,
	}, argID, dsTypeIdHelp)
}

func (SoftwareModule) AddUpdateFlags(fs *flag.FlagSet, u *swmods.UpdateData) {
	fs.Var(optString{ValPtrPtr: &u.Description}, argDescription, "Updated description of the software module")
	fs.Var(optString{ValPtrPtr: &u.Vendor}, argVendor, "Updated vendor of the software module")
}

func (SoftwareModule) CanCreate(c *swmods.CreateData) error {
	if c.Name == "" {
		return errSwModNameRequired
	}

	if c.Version == "" {
		return errSwModVersionRequired
	}

	if c.Type == "" {
		return errSwModTypeRequired
	}

	return nil
}

func (SoftwareModule) CanSelect(id swmods.ID) error {
	if id == 0 {
		return errSwModIdRequired
	}

	return nil
}

func (SoftwareModule) Create(ctx context.Context, cli *mgmtapi.Client, cs []swmods.CreateData) ([]swmods.Data, error) {
	return cli.SoftwareModuleModel().Create(ctx, cli.Client, cs)
}

func (SoftwareModule) Get(ctx context.Context, cli *mgmtapi.Client, id swmods.ID) (*swmods.Data, error) {
	return cli.SoftwareModuleModel().Ref(id).Get(ctx, cli.Client)
}

func (SoftwareModule) Update(ctx context.Context, cli *mgmtapi.Client, id swmods.ID, up *swmods.UpdateData) (*swmods.Data, error) {
	return cli.SoftwareModuleModel().Ref(id).Update(ctx, cli.Client, up)
}

func (SoftwareModule) Delete(ctx context.Context, cli *mgmtapi.Client, id swmods.ID) error {
	return cli.SoftwareModuleModel().Ref(id).Delete(ctx, cli.Client)
}

func (SoftwareModule) Show(ctx context.Context, cli *mgmtapi.Client, r *swmods.Data) error {
	_, w, _ := cmdr.Stdio(ctx)

	_, _ = fmt.Fprintf(w, fmtKeyValue, fldID, r.ID)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldName, r.Name)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldVendor, r.Vendor)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldVersion, r.Version)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldType, r.Type)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDeleted, r.Deleted)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDescription, r.Description)

	return nil
}

func (SoftwareModule) Find(ctx context.Context, cli *mgmtapi.Client, opts restapi.FindOptions) (*restapi.Found[swmods.FindData], error) {
	return cli.SoftwareModuleModel().Find(ctx, cli.Client, opts)
}

func (SoftwareModule) ListHeader(w io.Writer) error {
	_, err := fmt.Fprintf(w, fmtFields5, fldID, fldName, fldVendor, fldVersion, fldType)

	return err
}

func (SoftwareModule) ListItem(w io.Writer, f *swmods.FindData) error {
	_, err := fmt.Fprintf(w, fmtFields5, f.ID, f.Name, f.Vendor, f.Version, f.Type)

	return err
}

func (SoftwareModule) MetaDataFromArgs(args []string) ([]swmods.MetaDataEntry, error) {
	// TODO: how to configure TargetVisible?
	return metaDataFromArgs(args, func(k, v string) swmods.MetaDataEntry {
		return swmods.MetaDataEntry{Key: k, Value: v}
	})
}

func (SoftwareModule) CreateMetaData(ctx context.Context, cli *mgmtapi.Client, id swmods.ID, entries []swmods.MetaDataEntry) ([]swmods.MetaDataEntry, error) {
	return cli.SoftwareModuleModel().Ref(id).MetaDataModel().Create(ctx, cli.Client, entries)
}

func (SoftwareModule) ShowMetaData(ctx context.Context, cli *mgmtapi.Client, entries []swmods.MetaDataEntry) error {
	_, w, _ := cmdr.Stdio(ctx)
	tw := tabwriter.NewWriter(w, 1, 4, 1, ' ', 0)

	_, _ = fmt.Fprintf(tw, fmtFields3, attrKey, attrValue, attrTargetVisible)
	for _, e := range entries {
		_, _ = fmt.Fprintf(tw, fmtFields3, e.Key, e.Value, e.TargetVisible)
	}

	return tw.Flush()
}
