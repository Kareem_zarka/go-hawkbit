// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

const (
	argAddress           = "address"
	argDescription       = "description"
	argID                = "id"
	argKey               = "key"
	argMandatoryModules  = "mandatory-module-types"
	argMaxAssignments    = "max-assignments"
	argModules           = "modules"
	argName              = "name"
	argOptionalModules   = "optional-module-types"
	argRequestAttrs      = "request-attrs"
	argRequiredMigration = "required-migration"
	argSecurityToken     = "security-token"
	argType              = "type"
	argVendor            = "vendor"
	argVersion           = "version"
)
