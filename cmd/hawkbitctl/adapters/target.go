// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"text/tabwriter"
	"time"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/targets"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

const (
	targetIdHelp   = "Target ID, also known as Controller ID"
	cmdNameTarget  = "target"
	cmdNameTargets = "targets"
)

var (
	errTargetIdRequired = errors.New("target ID required")
)

// Target is the hawkbitctl adapter for mgmtapi.Target type.
type Target struct{}

func (Target) SingularName() string {
	return cmdNameTarget
}

func (Target) PluralName() string {
	return cmdNameTargets
}

func (Target) KnownFields() []string {
	return []string{
		jsControllerID,
		jsName,
		jsDescription,
		jsAddress,
		jsIpAddress,
		jsInstalledAt,
		jsLastControllerRequestAt,
		jsRequestAttributes,
		jsSecurityToken,
		jsUpdateStatus,

		jsCreatedBy,
		jsCreatedAt,
		jsLastModifiedAt,
		jsLastModifiedBy,
	}
}

func (Target) AddCreateFlags(fs *flag.FlagSet, c *targets.CreateData) {
	fs.Var(&c.ID, argID, targetIdHelp)
	fs.StringVar(&c.Name, argName, "", "Name of the new target")
	fs.StringVar(&c.Description, argDescription, "", "Description of the new target")
	fs.StringVar(&c.Address, argAddress, "", "Address of the the new target")
	fs.StringVar(&c.SecurityToken, argSecurityToken, "", "Authentication token of the new target")
}

func (Target) AddSelectFlags(fs *flag.FlagSet, id *targets.ID) {
	fs.Var(id, argID, targetIdHelp)
}

func (Target) AddUpdateFlags(fs *flag.FlagSet, u *targets.UpdateData) {
	fs.Var(optString{ValPtrPtr: &u.Name}, argName, "Updated name of the target")
	fs.Var(optString{ValPtrPtr: &u.Description}, argDescription, "Updated description of the target")
	fs.Var(optString{ValPtrPtr: &u.Address}, argAddress, "Updated address of the target")
	fs.Var(optString{ValPtrPtr: &u.SecurityToken}, argSecurityToken, "Updated authentication token of the target")
	fs.Var(optBool{ValPtrPtr: &u.RequestAttrs}, argRequestAttrs, "Request re-transmission of target attributes")
}

func (Target) CanSelect(id targets.ID) error {
	if id == "" {
		return errTargetIdRequired
	}

	return nil
}

func (Target) CanCreate(target *targets.CreateData) error {
	if target.ID == "" {
		return errTargetIdRequired
	}

	return nil
}

func (Target) Create(ctx context.Context, cli *mgmtapi.Client, items []targets.CreateData) ([]targets.Data, error) {
	return cli.TargetModel().Create(ctx, cli.Client, items)
}

func (Target) Get(ctx context.Context, cli *mgmtapi.Client, id targets.ID) (*targets.Data, error) {
	return cli.TargetModel().Ref(id).Get(ctx, cli.Client)
}

func (Target) Update(ctx context.Context, cli *mgmtapi.Client, id targets.ID, up *targets.UpdateData) (*targets.Data, error) {
	return cli.TargetModel().Ref(id).Update(ctx, cli.Client, up)
}

func (Target) Delete(ctx context.Context, cli *mgmtapi.Client, id targets.ID) error {
	return cli.TargetModel().Ref(id).Delete(ctx, cli.Client)
}

func (Target) Show(ctx context.Context, cli *mgmtapi.Client, r *targets.Data) error {
	_, w, _ := cmdr.Stdio(ctx)

	_, _ = fmt.Fprintf(w, fmtKeyValue, fldID, r.ID)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldName, r.Name)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDescription, r.Description)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldAddress, r.Address)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldIpAddress, r.IpAddress)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldSecurityToken, r.SecurityToken)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldUpdateStatus, r.UpdateStatus)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldRequestAttrs, r.RequestAttrs)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldInstalledAt, secTime(r.InstalledAt))
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldLastControllerRequestAt, secTime(r.LastControllerRequestAt))

	return nil
}

func (Target) Find(ctx context.Context, cli *mgmtapi.Client, opts restapi.FindOptions) (*restapi.Found[targets.FindData], error) {
	return cli.TargetModel().Find(ctx, cli.Client, opts)
}

func (Target) ListHeader(w io.Writer) error {
	_, err := fmt.Fprintf(w, fmtFields7, fldID, fldName, fldAddress, fldIpAddress, fldLastControllerRequestAt, fldInstalledAt, fldUpdateStatus)

	return err
}

func (Target) ListItem(w io.Writer, f *targets.FindData) error {
	_, err := fmt.Fprintf(w, fmtFields7, f.ID, f.Name, f.Address, f.IpAddress, secTime(f.LastControllerRequestAt), secTime(f.InstalledAt), f.UpdateStatus)

	return err
}

func secTime(sec int64) string {
	if sec == 0 {
		return "never"
	}

	return time.Unix(sec, 0).String()
}

func (Target) MetaDataFromArgs(args []string) ([]targets.MetaDataEntry, error) {
	return metaDataFromArgs(args, func(k, v string) targets.MetaDataEntry {
		return targets.MetaDataEntry{Key: k, Value: v}
	})
}

func (Target) CreateMetaData(ctx context.Context, cli *mgmtapi.Client, id targets.ID, entries []targets.MetaDataEntry) ([]targets.MetaDataEntry, error) {
	return cli.TargetModel().Ref(id).MetaDataModel().Create(ctx, cli.Client, entries)
}

func (Target) ShowMetaData(ctx context.Context, cli *mgmtapi.Client, entries []targets.MetaDataEntry) error {
	_, w, _ := cmdr.Stdio(ctx)
	tw := tabwriter.NewWriter(w, 1, 4, 1, ' ', 0)

	_, _ = fmt.Fprintf(tw, fmtFields2, attrKey, attrValue)
	for _, e := range entries {
		_, _ = fmt.Fprintf(tw, fmtFields2, e.Key, e.Value)
	}

	return tw.Flush()
}
