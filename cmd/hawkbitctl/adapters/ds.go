// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"strings"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dses"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

const (
	dsIdHelp    = "Distribution set ID"
	cmdNameDs   = "distribution-set"
	cmdNameDses = "distribution-sets"
)

var (
	errDsIdRequired   = errors.New("distribution set ID required")
	errDsNameRequired = errors.New("distribution set name required")
)

// DistributionSet is the hawkbitctl adapter for distribution set types.
type DistributionSet struct {
	SwModMapper IdMapper[swmods.ID, string, struct{}]
	DsMapper    IdMapper[dses.ID, string, struct{}]
}

func (DistributionSet) SingularName() string {
	return cmdNameDs
}

func (DistributionSet) PluralName() string {
	return cmdNameDses
}

func (DistributionSet) KnownFields() []string {
	return []string{
		jsID,
		jsName,
		jsVersion,
		jsType,
		jsComplete,
		jsDescription,
		jsRequiredMigrationStep,

		jsCreatedBy,
		jsCreatedAt,
		jsLastModifiedAt,
		jsLastModifiedBy,
		jsDeleted,
	}
}

func (a DistributionSet) AddCreateFlags(fs *flag.FlagSet, c *dses.CreateData) {
	fs.StringVar(&c.Name, argName, "", "Name of the new distribution set")
	fs.StringVar(&c.Description, argDescription, "", "Description of the new distribution set")
	fs.StringVar(&c.Version, argVersion, "", "Version of the new distribution set")
	fs.StringVar(&c.Type, argType, "", "Type of the new distribution set")
	fs.BoolVar(&c.RequiredMigrationStep, argRequiredMigration, false, "Required migration flag of the new distribution set")
	fs.Var(IdRefListValue[swmods.ID, struct{}]{IdRefListPtr: &c.Modules, Mapper: a.SwModMapper}, argModules, "Software modules of the new distribution set")
}

func (DistributionSet) AddUpdateFlags(fs *flag.FlagSet, u *dses.UpdateData) {
	fs.Var(optString{ValPtrPtr: &u.Name}, argName, "Updated name of the distribution set")
	fs.Var(optString{ValPtrPtr: &u.Description}, argDescription, "Updated description of the distribution set")
	fs.Var(optString{ValPtrPtr: &u.Version}, argVersion, "Updated version of the distribution set")
	fs.Var(optBool{ValPtrPtr: &u.RequiredMigrationStep}, argRequiredMigration, "Updated required migration step flag of the distribution set")
}

func (cmd DistributionSet) AddSelectFlags(fs *flag.FlagSet, id *dses.ID) {
	fs.Var(IdValue[dses.ID, *dses.ID, string, struct{}]{
		IdPtr:  id,
		Mapper: cmd.DsMapper,
	}, argID, dsTypeIdHelp)
}

func (DistributionSet) CanCreate(c *dses.CreateData) error {
	if c.Name == "" {
		return errDsNameRequired
	}

	return nil
}

func (DistributionSet) CanSelect(id dses.ID) error {
	if id == 0 {
		return errDsIdRequired
	}

	return nil
}

func (DistributionSet) Create(ctx context.Context, cli *mgmtapi.Client, cs []dses.CreateData) ([]dses.Data, error) {
	return cli.DistributionSetModel().Create(ctx, cli.Client, cs)
}

func (DistributionSet) Delete(ctx context.Context, cli *mgmtapi.Client, id dses.ID) error {
	return cli.DistributionSetModel().Ref(id).Delete(ctx, cli.Client)
}

func (DistributionSet) Get(ctx context.Context, cli *mgmtapi.Client, id dses.ID) (*dses.Data, error) {
	return cli.DistributionSetModel().Ref(id).Get(ctx, cli.Client)
}

func (DistributionSet) Update(ctx context.Context, cli *mgmtapi.Client, id dses.ID, up *dses.UpdateData) (*dses.Data, error) {
	return cli.DistributionSetModel().Ref(id).Update(ctx, cli.Client, up)
}

func (DistributionSet) Show(ctx context.Context, cli *mgmtapi.Client, r *dses.Data) error {
	_, w, _ := cmdr.Stdio(ctx)

	_, _ = fmt.Fprintf(w, fmtKeyValue, fldID, r.ID)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldName, r.Name)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldType, r.Type)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldVersion, r.Version)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDeleted, r.Deleted)
	_, _ = fmt.Fprintf(w, fmtKeyValue, fldDescription, r.Description)

	_, _ = fmt.Fprintf(w, fmtKeyList, fldSoftwareModules, len(r.Modules))

	for i := range r.Modules {
		m := &r.Modules[i]

		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldName, m.Name)
		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldVendor, m.Vendor)
		_, _ = fmt.Fprintf(w, fmtInsetKeyValue, fldVersion, m.Version)
	}

	return nil
}

func (DistributionSet) Find(ctx context.Context, cli *mgmtapi.Client, opts restapi.FindOptions) (*restapi.Found[dses.FindData], error) {
	return cli.DistributionSetModel().Find(ctx, cli.Client, opts)
}

func (DistributionSet) ListHeader(w io.Writer) error {
	_, err := fmt.Fprintf(w, fmtFields3, fldID, fldKey, fldVersion)

	return err
}

func (DistributionSet) ListItem(w io.Writer, f *dses.FindData) error {
	_, err := fmt.Fprintf(w, fmtFields3, f.ID, f.Name, f.Version)

	return err
}

func (DistributionSet) MetaDataFromArgs(args []string) ([]dses.MetaDataEntry, error) {
	return metaDataFromArgs(args, func(k, v string) dses.MetaDataEntry {
		return dses.MetaDataEntry{Key: k, Value: v}
	})
}

func (DistributionSet) CreateMetaData(ctx context.Context, cli *mgmtapi.Client, id dses.ID, entries []dses.MetaDataEntry) ([]dses.MetaDataEntry, error) {
	return cli.DistributionSetModel().Ref(id).MetaDataModel().Create(ctx, cli.Client, entries)
}

func (DistributionSet) ShowMetaData(ctx context.Context, cli *mgmtapi.Client, entries []dses.MetaDataEntry) error {
	_, w, _ := cmdr.Stdio(ctx)
	tw := tabwriter.NewWriter(w, 1, 4, 1, ' ', 0)

	_, _ = fmt.Fprintf(tw, fmtFields2, attrKey, attrValue)
	for _, e := range entries {
		_, _ = fmt.Fprintf(tw, fmtFields2, e.Key, e.Value)
	}

	return tw.Flush()
}

func metaDataFromArgs[metaT any](args []string, makeEntryFn func(k, v string) metaT) ([]metaT, error) {
	entries := make([]metaT, 0, len(args))

	for _, arg := range args {
		key, value, ok := strings.Cut(arg, "=")
		if !ok {
			return nil, fmt.Errorf("unexpected meta-data pair: %q", arg)
		}

		entries = append(entries, makeEntryFn(key, value))
	}

	return entries, nil
}
