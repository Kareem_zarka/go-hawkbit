// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package adapters

import "fmt"

const (
	strTrue      = "true"
	strFalse     = "false"
	strUnchanged = "unchanged"
)

type optString struct {
	ValPtrPtr **string
}

// String returns true, false or unchanged.
func (o optString) String() string {
	if o.ValPtrPtr == nil || *o.ValPtrPtr == nil {
		return ""
	}

	return **o.ValPtrPtr
}

// Set sets the value to the given string.
func (o optString) Set(s string) error {
	if o.ValPtrPtr == nil {
		panic("OptionalString must be initialized with a valid pointer")
	}

	if *o.ValPtrPtr == nil {
		*o.ValPtrPtr = new(string)
	}

	**o.ValPtrPtr = s

	return nil
}

type optBool struct {
	ValPtrPtr **bool
}

// String returns true, false or unchanged.
func (o optBool) String() string {
	if o.ValPtrPtr == nil || *o.ValPtrPtr == nil {
		return strUnchanged
	}

	if **o.ValPtrPtr {
		return strTrue
	} else {
		return strFalse
	}
}

// Set sets the value to the given string.
func (o optBool) Set(s string) error {
	if o.ValPtrPtr == nil {
		panic("OptionalBool must be initialized with a valid pointer")
	}

	switch s {
	case strTrue:
		if *o.ValPtrPtr == nil {
			*o.ValPtrPtr = new(bool)
		}

		**o.ValPtrPtr = true
	case strFalse:
		if *o.ValPtrPtr == nil {
			*o.ValPtrPtr = new(bool)
		}

		**o.ValPtrPtr = false
	default:
		return fmt.Errorf("not a boolean value: %s", s)
	}

	return nil
}

// IsBoolFlag implements optional co-interface to flag.Value.
func (optBool) IsBoolFlag() bool {
	return true
}
