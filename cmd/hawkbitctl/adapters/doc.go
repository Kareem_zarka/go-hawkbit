// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package adapters provides glue logic for interfacing specific HawkBit types with the command line interface of hawkbitctl.
package adapters
