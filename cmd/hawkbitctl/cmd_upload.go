// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type uploadAdapter[readT any, cmdStateT any] interface {
	SingularName() string

	AddUploadFlags(*flag.FlagSet, *cmdStateT)
	CanUpload(*cmdStateT) error
	Upload(context.Context, *mgmtapi.Client, *cmdStateT) (*readT, error)

	// TODO: drop io.Writer and use cmdr.Stdio on the context.
	Show(context.Context, *mgmtapi.Client, *readT) error
}

// uploadCmd provides a command for uploading artifacts.
type uploadCmd[readT any, cmdStateT any, adapterT uploadAdapter[readT, cmdStateT]] struct {
	adapter adapterT
}

// NewUploadCmd returns a new upload command with the given adapter.
func NewUploadCmd[readT any, cmdStateT any, adapterT uploadAdapter[readT, cmdStateT]](a adapterT) cmdr.Cmd {
	return &uploadCmd[readT, cmdStateT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd *uploadCmd[readT, cmdStateT, adapterT]) OneLiner() string {
	return fmt.Sprintf("upload %s", cmd.adapter.SingularName())
}

func (cmd *uploadCmd[readT, cmdStateT, adapterT]) Run(ctx context.Context, args []string) error {
	var cmdState cmdStateT

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	cmd.adapter.AddUploadFlags(fs, &cmdState)

	_, _, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := cmd.adapter.CanUpload(&cmdState); err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	data, err := cmd.adapter.Upload(ctx, cli, &cmdState)
	if err != nil {
		return err
	}

	return cmd.adapter.Show(ctx, cli, data)
}
