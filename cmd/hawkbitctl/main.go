// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package main implements hawkbitctl - a command line utility exposing HawkBit
// management APIs to shell programs.
package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"

	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/adapters"
	"gitlab.com/zygoon/go-hawkbit/cmd/hawkbitctl/mappers"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/artifacts"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dses"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/dstypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmodtypes"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/targets"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

var errUnexpectedArg = errors.New("unexpected argument, try passing --help")

func main() {
	cmdr.RunMain(mainCmd{}, os.Args, cmdr.WithPanicBehavior(cmdr.RecoverTraceExit))
}

type mainCmd struct{}

func (mainCmd) Run(ctx context.Context, args []string) error {
	var opts options

	ctx = withOptions(ctx, &opts)

	var verbose bool

	_, _, stderr := cmdr.Stdio(ctx)
	fs := flag.NewFlagSet("hawkbitctl", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.BoolVar(&verbose, "v", false, "Verbose mode")
	opts.Vars(fs)

	fs.Usage = func() {
		w := fs.Output()
		tw := tabwriter.NewWriter(w, 4, 4, 1, ' ', 0)

		_, _ = fmt.Fprintf(w, "Usage: %s [ACCOUNT-OPTIONS] <COMMAND>\n", fs.Name())
		_, _ = fmt.Fprintf(w, "\nHawkBit account options:\n")

		fs.VisitAll(func(f *flag.Flag) {
			_, _ = fmt.Fprintf(tw, "  -%s\t%s\t\n", f.Name, f.Usage)
		})

		_ = tw.Flush()
		_, _ = fmt.Fprintf(w, "\nAccount configuration may be provided as environment variables\nHAWKBIT_{URL,USERNAME,PASSWORD,TENANT}\n")
	}

	if err := fs.Parse(args); err != nil {
		return err
	}

	var tgt adapters.Target

	var smt adapters.SoftwareModuleType

	var sm adapters.SoftwareModule = adapters.SoftwareModule{
		SwModMapper: mappers.NewSoftwareModuleNameVersionMapper(ctx, loadClient),
	}

	var dst adapters.DistributionSetType = adapters.DistributionSetType{
		SwModTypeMapper: mappers.NewSoftwareModuleTypeKeyMapper(ctx, loadClient),
	}

	var ds adapters.DistributionSet = adapters.DistributionSet{
		SwModMapper: mappers.NewSoftwareModuleNameVersionMapper(ctx, loadClient),
		DsMapper:    mappers.NewDistributionSetNameVersionMapper(ctx, loadClient),
	}

	var arts adapters.Artifact = adapters.Artifact{
		SwModMapper:    mappers.NewSoftwareModuleNameVersionMapper(ctx, loadClient),
		ArtifactMapper: mappers.NewArtifactNameMapper[*adapters.ArtifactSelector](ctx, loadClient),
	}

	createCmds := map[string]cmdr.Cmd{
		tgt.SingularName(): NewCreateItemOrMetaCmd[targets.ID, targets.CreateData, targets.Data, targets.MetaDataEntry](tgt),
		smt.SingularName(): NewCreateItemCmd[swmodtypes.CreateData, swmodtypes.Data](smt),
		sm.SingularName():  NewCreateItemOrMetaCmd[swmods.ID, swmods.CreateData, swmods.Data, swmods.MetaDataEntry](sm),
		dst.SingularName(): NewCreateItemCmd[dstypes.CreateData, dstypes.Data](dst),
		ds.SingularName():  NewCreateItemOrMetaCmd[dses.ID, dses.CreateData, dses.Data, dses.MetaDataEntry](ds),
	}

	deleteCmds := map[string]cmdr.Cmd{
		tgt.SingularName():  NewDeleteItemCmd[targets.ID](tgt),
		smt.SingularName():  NewDeleteItemCmd[swmodtypes.ID](smt),
		sm.SingularName():   NewDeleteItemCmd[swmods.ID](sm),
		dst.SingularName():  NewDeleteItemCmd[dstypes.ID](dst),
		ds.SingularName():   NewDeleteItemCmd[dses.ID](ds),
		arts.SingularName(): NewDeleteItemCmd[adapters.ArtifactSelector](arts),
	}

	showCmds := map[string]cmdr.Cmd{
		tgt.SingularName():  NewShowItemCmd[targets.Data, targets.ID](tgt),
		smt.SingularName():  NewShowItemCmd[swmodtypes.Data, swmodtypes.ID](smt),
		sm.SingularName():   NewShowItemCmd[swmods.Data, swmods.ID](sm),
		dst.SingularName():  NewShowItemCmd[dstypes.Data, dstypes.ID](dst),
		ds.SingularName():   NewShowItemCmd[dses.Data, dses.ID](ds),
		arts.SingularName(): NewShowItemCmd[artifacts.Data, adapters.ArtifactSelector](arts),
	}

	editCmds := map[string]cmdr.Cmd{
		tgt.SingularName(): NewEditItemCmd[targets.Data, targets.UpdateData, targets.ID](tgt),
		smt.SingularName(): NewEditItemCmd[swmodtypes.Data, swmodtypes.UpdateData, swmodtypes.ID](smt),
		sm.SingularName():  NewEditItemCmd[swmods.Data, swmods.UpdateData, swmods.ID](sm),
		dst.SingularName(): NewEditItemCmd[dstypes.Data, dstypes.UpdateData, dstypes.ID](dst),
		ds.SingularName():  NewEditItemCmd[dses.Data, dses.UpdateData, dses.ID](ds),
	}

	findCmds := map[string]cmdr.Cmd{
		tgt.PluralName(): NewFindItemsCmd[targets.FindData](tgt),
		smt.PluralName(): NewFindItemsCmd[swmodtypes.FindData](smt),
		sm.PluralName():  NewFindItemsCmd[swmods.FindData](sm),
		dst.PluralName(): NewFindItemsCmd[dstypes.FindData](dst),
		ds.PluralName():  NewFindItemsCmd[dses.FindData](ds),
	}

	listCmds := map[string]cmdr.Cmd{
		arts.PluralName(): NewListItemsCmd[artifacts.Data, adapters.ArtifactListState](arts),
	}

	uploadCmd := NewUploadCmd[artifacts.Data, adapters.ArtifactUploadState](arts)

	r := router.Cmd{
		Name: fs.Name(),
		Commands: map[string]cmdr.Cmd{
			"create": &router.Cmd{
				Name:         "hawkbitctl create",
				OneLinerText: "create objects",
				Commands:     createCmds,
			},
			"c": &router.Cmd{
				Name:         "hawkbitctl c",
				OneLinerText: "alias for create",
				Commands:     createCmds,
			},
			"delete": &router.Cmd{
				Name:         "hawkbitctl delete",
				OneLinerText: "create objects",
				Commands:     deleteCmds,
			},
			"d": &router.Cmd{
				Name:         "hawkbitctl d",
				OneLinerText: "alias for delete",
				Commands:     deleteCmds,
			},
			"show": &router.Cmd{
				Name:         "hawkbitctl show",
				OneLinerText: "show objects",
				Commands:     showCmds,
			},
			"s": &router.Cmd{
				Name:         "hawkbitctl s",
				OneLinerText: "alias for show",
				Commands:     showCmds,
			},
			"edit": &router.Cmd{
				Name:         "hawkbitctl edit",
				OneLinerText: "edit objects",
				Commands:     editCmds,
			},
			"e": &router.Cmd{
				Name:         "hawkbitctl e",
				OneLinerText: "alias for edit",
				Commands:     editCmds,
			},
			"find": &router.Cmd{
				Name:         "hawkbitctl find",
				OneLinerText: "find or list objects",
				Commands:     findCmds,
			},
			"f": &router.Cmd{
				Name:         "hawkbitctl f",
				OneLinerText: "alias for find",
				Commands:     findCmds,
			},
			"list": &router.Cmd{
				Name:         "hawkbitctl list",
				OneLinerText: "list objects",
				Commands:     listCmds,
			},
			"l": &router.Cmd{
				Name:         "hawkbitctl l",
				OneLinerText: "alias for list",
				Commands:     listCmds,
			},
			"up":     uploadCmd,
			"upload": uploadCmd,
		},
	}

	err := r.Run(ctx, fs.Args())

	var errInfo *restapi.Error
	if err != nil && verbose && errors.As(err, &errInfo) {
		_, _ = fmt.Fprintf(stderr, "HawkBit error code: %s\n", errInfo.Code)
		_, _ = fmt.Fprintf(stderr, "HawkBit error class: %s\n", errInfo.Class)
	}

	return err
}
