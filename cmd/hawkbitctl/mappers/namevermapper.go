// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mappers

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
	"gitlab.com/zygoon/go-hawkbit/pkg/restapi"
)

var errExpectedNameVersionSyntax = errors.New("expected name:version syntax")

// NameVersionMapper maps IDs to the name:version string.
type NameVersionMapper[
	IdT interface{ ~int },
	ReadT interface {
		// Due to https://github.com/golang/go/issues/48522
		// we cannot just access Name and Version so we use a
		// helper method that provides the same information.
		AlternateID() string
	},
	FindT interface {
		// Due to https://github.com/golang/go/issues/48522
		// we cannot just access Name and Version so we use a helper
		// method that provides the same information.
		PrimaryID() IdT
	},
	RefT interface {
		Get(context.Context, *restapi.Client) (*ReadT, error)
	},
	ModelT interface {
		Ref(IdT) RefT
		Find(context.Context, *restapi.Client, restapi.FindOptions) (*restapi.Found[FindT], error)
	},
] struct {
	ctx         context.Context
	loader      ClientLoader
	modelFn     func(*mgmtapi.Client) ModelT
	notFoundErr error
	// TODO: cache requests?
}

func NewNameVersionMapper[
	IdT interface{ ~int },
	ReadT interface {
		// Due to https://github.com/golang/go/issues/48522
		// we cannot just access Name and Version so we use a
		// helper method that provides the same information.
		AlternateID() string
	},
	FindT interface {
		// Due to https://github.com/golang/go/issues/48522
		// we cannot just access Name and Version so we use a helper
		// method that provides the same information.
		PrimaryID() IdT
	},
	RefT interface {
		Get(context.Context, *restapi.Client) (*ReadT, error)
	},
	ModelT interface {
		Ref(IdT) RefT
		Find(context.Context, *restapi.Client, restapi.FindOptions) (*restapi.Found[FindT], error)
	},
](ctx context.Context, loader ClientLoader, modelFn func(*mgmtapi.Client) ModelT, notFoundErr error) *NameVersionMapper[IdT, ReadT, FindT, RefT, ModelT] {
	return &NameVersionMapper[IdT, ReadT, FindT, RefT, ModelT]{
		ctx:         ctx,
		loader:      loader,
		modelFn:     modelFn,
		notFoundErr: notFoundErr,
	}
}

func (m *NameVersionMapper[SelectorT, ReadT, FindT, RefT, ModelT]) ToPrimary(nameVer string, env struct{}) (SelectorT, error) {
	name, ver, ok := strings.Cut(nameVer, ":")
	if !ok {
		return 0, errExpectedNameVersionSyntax
	}

	cli, err := m.loader(m.ctx)
	if err != nil {
		return 0, err
	}

	opts := restapi.FindOptions{
		Limit: 1,
		// Feed Item Query Language expression.
		// FIXME: there's no escaping for name and version here.
		RawQ: fmt.Sprintf("name==%s;version==%s", name, ver),
	}

	found, err := m.modelFn(cli).Find(m.ctx, cli.Client, opts)
	if err != nil {
		return 0, err
	}

	if found.Total != 1 {
		return 0, m.notFoundErr
	}

	return found.Content[0].PrimaryID(), nil
}

func (m *NameVersionMapper[SelectorT, ReadT, FindT, RefT, ModelT]) ToAlternate(id SelectorT, env struct{}) (string, error) {
	cli, err := m.loader(m.ctx)
	if err != nil {
		return "", err
	}

	obj, err := m.modelFn(cli).Ref(id).Get(m.ctx, cli.Client)
	if err != nil {
		return "", err
	}

	return (*obj).AlternateID(), nil
}
