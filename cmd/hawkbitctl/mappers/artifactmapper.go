// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mappers

import (
	"context"
	"errors"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/artifacts"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi/swmods"
)

var (
	errArtifactNotFound = errors.New("artifact not found")
)

// ArtifactMapperEnv provides a way to find software module ID.
type ArtifactMapperEnv interface {
	SoftwareModuleID() (swmods.ID, error)
}

// SoftwareModuleNameVersionMapper maps software module IDs to the name@version string.
type ArtifactNameMapper[mapEnvT ArtifactMapperEnv] struct {
	ctx    context.Context
	loader ClientLoader
	// TODO: cache requests?
}

func NewArtifactNameMapper[mapEnvT ArtifactMapperEnv](ctx context.Context, loader ClientLoader) *ArtifactNameMapper[mapEnvT] {
	return &ArtifactNameMapper[mapEnvT]{ctx: ctx, loader: loader}
}

// ToPrimary returns the ID of an artifact with the given filename.
func (m *ArtifactNameMapper[mapEnvT]) ToPrimary(filename string, env mapEnvT) (artifacts.ID, error) {
	moduleId, err := env.SoftwareModuleID()
	if err != nil {
		return 0, err
	}

	cli, err := m.loader(m.ctx)
	if err != nil {
		return 0, err
	}

	all, err := cli.SoftwareModuleModel().Ref(moduleId).ArtifactModel().GetAll(m.ctx, cli.Client)
	if err != nil {
		return 0, err
	}

	for i := range all {
		if a := &all[i]; a.ProvidedFileName == filename {
			return a.ID, nil
		}
	}

	return 0, errArtifactNotFound
}

func (m *ArtifactNameMapper[mapEnvT]) ToAlternate(id artifacts.ID, env mapEnvT) (string, error) {
	moduleId, err := env.SoftwareModuleID()
	if err != nil {
		return "", err
	}

	cli, err := m.loader(m.ctx)
	if err != nil {
		return "", err
	}

	a, err := cli.SoftwareModuleModel().Ref(moduleId).ArtifactModel().Ref(id).Get(m.ctx, cli.Client)
	if err != nil {
		return "", err
	}

	return a.ProvidedFileName, nil
}
