// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type showAdapter[readT any, selectorT any] interface {
	SingularName() string

	AddSelectFlags(*flag.FlagSet, *selectorT)
	CanSelect(selectorT) error
	Get(context.Context, *mgmtapi.Client, selectorT) (*readT, error)
	Show(context.Context, *mgmtapi.Client, *readT) error
}

// showItemCmd provides a command for showing an item of a given type.
type showItemCmd[readT any, selectorT any, adapterT showAdapter[readT, selectorT]] struct {
	adapter adapterT
}

// NewEditItemCmd returns a new show item command with the given adapter.
func NewShowItemCmd[readT, selectorT any, adapterT showAdapter[readT, selectorT]](a adapterT) cmdr.Cmd {
	return &showItemCmd[readT, selectorT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd showItemCmd[readT, selectorT, adapterT]) OneLiner() string {
	return fmt.Sprintf("show information about a specific %s", cmd.adapter.SingularName())
}

// Run retrieves an item through the HawkBit API and displays it.
func (cmd showItemCmd[readT, selectorT, adapterT]) Run(ctx context.Context, args []string) error {
	var sel selectorT

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	cmd.adapter.AddSelectFlags(fs, &sel)

	_, _, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := cmd.adapter.CanSelect(sel); err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	it, err := cmd.adapter.Get(ctx, cli, sel)
	if err != nil {
		return err
	}

	return cmd.adapter.Show(ctx, cli, it)
}
