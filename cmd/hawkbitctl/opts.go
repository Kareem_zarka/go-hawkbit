// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type optionsKey struct{}

type options struct {
	addr       string
	user, pass string
	tenant     string
}

func (opts *options) Vars(fs *flag.FlagSet) {
	fs.StringVar(&opts.addr, "url", os.Getenv("HAWKBIT_URL"), "URL of your HawkBit server")
	fs.StringVar(&opts.user, "username", os.Getenv("HAWKBIT_USERNAME"), "HawkBit account name (username)")
	fs.StringVar(&opts.pass, "password", os.Getenv("HAWKBIT_PASSWORD"), "HawkBit account password")
	fs.StringVar(&opts.tenant, "tenant", os.Getenv("HAWKBIT_TENANT"), "Tenant name on a multi-tenant instance")
}

func (opts *options) Client() (*mgmtapi.Client, error) {
	if opts.addr == "" {
		return nil, errors.New("HawkBit instance address required")
	}

	if opts.user == "" || opts.pass == "" {
		return nil, errors.New("HawkBit account credentials required")
	}

	if opts.tenant != "" {
		return mgmtapi.NewTenantClient(opts.addr, opts.tenant, opts.user, opts.pass), nil
	}

	return mgmtapi.NewClient(opts.addr, opts.user, opts.pass), nil
}

func withOptions(parent context.Context, opts *options) context.Context {
	return context.WithValue(parent, optionsKey{}, opts)
}

func optionsFromCtx(ctx context.Context) (*options, error) {
	opts, ok := ctx.Value(optionsKey{}).(*options)
	if !ok {
		return nil, fmt.Errorf("cannot find hakwbit options in %v", ctx)
	}

	return opts, nil
}

// loadClient returns the client from the given context.
//
// This implements mappers.ClientLoader and allows delaying the construction of
// the HawkBit client, which requires credentials and URL, to the moment it is
// really needed.
func loadClient(ctx context.Context) (*mgmtapi.Client, error) {
	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	return opts.Client()
}
