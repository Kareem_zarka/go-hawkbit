// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type deleteAdapter[selectorT any] interface {
	SingularName() string

	AddSelectFlags(*flag.FlagSet, *selectorT)
	CanSelect(selectorT) error
	Delete(context.Context, *mgmtapi.Client, selectorT) error
}

// deleteItemCmd provides a command for deleting items of a given type.
type deleteItemCmd[selectorT any, adapterT deleteAdapter[selectorT]] struct {
	adapter adapterT
}

// NewDeleteItemCmd returns a new delete item command with the given adapter.
func NewDeleteItemCmd[selectorT any, adapterT deleteAdapter[selectorT]](a adapterT) cmdr.Cmd {
	return &deleteItemCmd[selectorT, adapterT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd deleteItemCmd[selectorT, adapterT]) OneLiner() string {
	return fmt.Sprintf("delete a specific %s", cmd.adapter.SingularName())
}

// Run deletes a specific item through the HawkBit API.
func (cmd deleteItemCmd[selectorT, adapterT]) Run(ctx context.Context, args []string) error {
	var sel selectorT

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	cmd.adapter.AddSelectFlags(fs, &sel)

	_, _, stderr := cmdr.Stdio(ctx)
	fs.SetOutput(stderr)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := cmd.adapter.CanSelect(sel); err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	return cmd.adapter.Delete(ctx, cli, sel)
}
