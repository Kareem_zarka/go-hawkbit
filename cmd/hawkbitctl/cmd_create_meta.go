// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"errors"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-hawkbit/pkg/mgmtapi"
)

type createItemOrMetaAdapter[selectorT, createT, readT, metaT any] interface {
	createAdapter[createT, readT]

	SingularName() string
	AddSelectFlags(*flag.FlagSet, *selectorT)
	CanSelect(selectorT) error
	MetaDataFromArgs([]string) ([]metaT, error)
	CreateMetaData(context.Context, *mgmtapi.Client, selectorT, []metaT) ([]metaT, error)
	ShowMetaData(context.Context, *mgmtapi.Client, []metaT) error
}

// createItemOrMetaCmd provides a command for creating items or meta-data entries of a given type.
type createItemOrMetaCmd[selectorT, createT, readT, metaT any] struct {
	adapter createItemOrMetaAdapter[selectorT, createT, readT, metaT]
}

// NewCreateItemCmd returns a new create item command with the given adapter.
func NewCreateItemOrMetaCmd[selectorT, createT, readT, metaT any](a createItemOrMetaAdapter[selectorT, createT, readT, metaT]) cmdr.Cmd {
	return &createItemOrMetaCmd[selectorT, createT, readT, metaT]{adapter: a}
}

// OneLiner returns a one-line description of the command.
func (cmd createItemOrMetaCmd[selectorT, createT, readT, metaT]) OneLiner() string {
	n := cmd.adapter.SingularName()

	return fmt.Sprintf("create a new %s or %s meta-data", n, n)
}

// Run creates a new item through the HawkBit API.
func (cmd createItemOrMetaCmd[selectorT, createT, readT, metaT]) Run(ctx context.Context, args []string) error {
	if len(args) == 0 || args[0] != "metadata" {
		createCmd := NewCreateItemCmd[createT, readT](cmd.adapter)

		return createCmd.Run(ctx, args)
	}

	// Drop "metadata" argument.
	args = args[1:]

	fs := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fs.Usage = func() {
		w := fs.Output()

		_, _ = fmt.Fprintf(w, "Usage: %s [OPTIONS] key=value [key2=value2...]\n", fs.Name())

		fs.PrintDefaults()

		_, _ = fmt.Fprintln(w, "Each key=value pair describes a single meta-data entry to create.")
	}

	_, _, stderr := cmdr.Stdio(ctx)

	fs.SetOutput(stderr)

	var sel selectorT

	cmd.adapter.AddSelectFlags(fs, &sel)

	if err := fs.Parse(args); err != nil {
		return err
	}

	if fs.NArg() == 0 {
		return errors.New("expected at least one metadata key=value pair")
	}

	if err := cmd.adapter.CanSelect(sel); err != nil {
		return err
	}

	entries, err := cmd.adapter.MetaDataFromArgs(fs.Args())
	if err != nil {
		return err
	}

	opts, err := optionsFromCtx(ctx)
	if err != nil {
		return err
	}

	cli, err := opts.Client()
	if err != nil {
		return err
	}

	entries, err = cmd.adapter.CreateMetaData(ctx, cli, sel, entries)
	if err != nil {
		return err
	}

	return cmd.adapter.ShowMetaData(ctx, cli, entries)
}
