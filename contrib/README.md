<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# HawkBit docker contrainer 

The `Dockerfile` in this directory allows building a snapshot of HawkBit  
that is suitable for testing go-hawkbit or light production use.

The container stores both the database and any uploaded artifacts in the docker
volume mounted at /data. RabbitMQ support is disabled, this disables DDF APIs.
