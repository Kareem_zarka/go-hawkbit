# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

ARG ARCH=

FROM golang:latest AS build
WORKDIR /src
COPY . .
RUN \
  if [ -f prebuilt/hawkbitctl ]; then \
    install -m 755 prebuilt/hawkbitctl /usr/local/bin/hawkbitctl; \
  else \
    go mod download; \
    CGO_ENABLED=0 go build -ldflags=-s -ldflags=-w -trimpath -o /usr/local/bin/hawkbitctl ./cmd/hawkbitctl; \
  fi;

FROM ubuntu:latest as ubuntu
WORKDIR /
RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y --no-install-recommends \
          ca-certificates \
      && apt-get clean -y \
      && apt-get autoremove -y \
      && rm -rf /tmp/* /var/tmp/* \
      && rm -rf /var/lib/apt/lists/*
COPY --from=build /usr/local/bin/hawkbitctl /usr/local/bin/hawkbitctl

FROM alpine:latest as alpine
WORKDIR /
COPY --from=build /usr/local/bin/hawkbitctl /usr/local/bin/hawkbitctl

FROM scratch as hawkbitctl
WORKDIR /
COPY --from=build /usr/local/bin/hawkbitctl /hawkbitctl
ENTRYPOINT [ "/hawkbitctl" ]
CMD [ "--help" ]
